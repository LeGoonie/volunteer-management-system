
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule, Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from "@agm/core";
import { NgxChartsModule } from "@swimlane/ngx-charts";

import { UserGuard } from './guards/user.guard';
import { EntityGuard } from './guards/entity.guard';
import { AdminGuard } from './guards/admin.guard';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RecupPassComponent } from './components/recup-pass/recup-pass.component';
import { NewPassComponent } from './components/new-pass/new-pass.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisterEntidadeComponent } from './components/register-entidade/register-entidade.component';
import { RegistrationChoiceComponent } from './components/registration-choice/registration-choice.component';
import { UsersListComponent } from './components/users/users-list/users-list.component';
import { EntitiesListComponent } from './components/entities-list/entities-list.component';
import { OtherProfileComponent } from './components/other-profile/other-profile.component';
import { SetRecupPassComponent } from './components/set-recup-pass/set-recup-pass.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { AdminPropComponent } from './components/admin-prop/admin-prop.component';
import { AdminPropListComponent } from './components/admin-prop/admin-prop-list/admin-prop-list.component';
import { VolunteeringJobsComponent } from './components/volunteering-jobs/volunteering-jobs.component';
import { VolunteeringJobsListComponent } from './components/volunteering-jobs/volunteering-jobs-list/volunteering-jobs-list.component';
import { ReceivedProposalsComponent } from './components/received-proposals/received-proposals.component';
import { ReceivedProposalsListComponent } from './components/received-proposals/received-proposals-list/received-proposals-list.component';
import { EntityPropsComponent } from './components/entity-props/entity-props.component';
import { EntityPropsListComponent } from './components/entity-props/entity-props-list/entity-props-list.component';
import { UserService } from './shared/user.service';
import { EntityService } from './shared/entity/entity.service';
import { VoluntaryService } from './shared/voluntary/voluntary.service';
import { ProposalService } from './shared/proposal/proposal.service';
import { NotificationService } from './shared/notification/notification.service';
import { ActivateComponent } from './components/activate/activate.component';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { VolunteeringHistoryListComponent } from './components/profile/volunteering-history-list/volunteering-history-list.component';
import { ProposalFormComponent } from './components/proposal-form/proposal-form.component';
import { VoluntaryFormComponent } from './components/voluntary-form/voluntary-form.component';
import { VoluntaryProfileComponent } from './components/voluntary-profile/voluntary-profile.component';
import { InvolvedVoluntariesComponent } from './components/voluntary-profile/involved-voluntaries/involved-voluntaries.component';
import { EditProposalComponent } from './components/edit-proposal/edit-proposal.component';
import { EditVoluntaryComponent } from './components/edit-voluntary/edit-voluntary.component';
import { ProposalProfileComponent } from './components/proposal-profile/proposal-profile.component';
import { NotificationComponent } from './components/notification/notification.component';
import { UsersComponent } from './components/users/users.component';
import { OtherProfileVoluntaryListComponent } from './components/other-profile/other-profile-voluntary-list/other-profile-voluntary-list.component';
import { StatsAdminComponent } from './components/stats-admin/stats-admin.component';
import { StatsVoluntariadosComponent } from './components/stats-admin/stats-voluntariados/stats-voluntariados.component';
import { StatsVoluntariesStateComponent } from './components/stats-admin/stats-voluntaries-state/stats-voluntaries-state.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'recupPass', component: RecupPassComponent},
  {path: 'recupPassConfirm/:token', component: SetRecupPassComponent},
  {path: 'newPass', component: NewPassComponent},
  {path: 'changePassword', component: ChangePasswordComponent},
  {path: 'profile', component: ProfileComponent, canActivate:[UserGuard]},
  {path: 'profile/:id', component: OtherProfileComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'registerEntidade', component: RegisterEntidadeComponent},
  {path: 'registerChoice', component: RegistrationChoiceComponent},
  {path: 'activate/:token', component: ActivateComponent},
  {path: 'usersList', component: UsersComponent, canActivate:[UserGuard]},
  {path: 'entitiesList', component: EntitiesListComponent, canActivate:[UserGuard]},
  {path: 'editProfile', component: EditProfileComponent, canActivate:[UserGuard]},
  {path: 'adminProposals', component: AdminPropComponent, canActivate:[UserGuard, AdminGuard]},
  {path: 'volunteeringJobs', component: VolunteeringJobsComponent},
  {path: 'receivedProposals', component: ReceivedProposalsComponent, canActivate:[UserGuard]},
  {path: 'proposalForm', component: ProposalFormComponent, canActivate:[UserGuard, EntityGuard]},
  {path: 'voluntaryForm', component: VoluntaryFormComponent},
  {path: 'myEntityProposals', component:EntityPropsComponent, canActivate:[UserGuard]},
  {path: 'voluntaryProfile/:id', component:VoluntaryProfileComponent},
  {path: 'myProposals', component:EntityPropsComponent, canActivate:[UserGuard, EntityGuard]},
  {path: 'editProposal/:id', component:EditProposalComponent, canActivate:[UserGuard, EntityGuard]},
  {path: 'editVoluntary/:id', component:EditVoluntaryComponent},
  {path: 'proposalProfile/:id', component:ProposalProfileComponent, canActivate:[UserGuard]},
  {path: 'proposalProfile/:id', component:ProposalProfileComponent, canActivate:[UserGuard]},
  {path: 'notification/:id', component:NotificationComponent, canActivate:[UserGuard]},
  {path: 'statsAdmin', component: StatsAdminComponent, canActivate:[AdminGuard]}
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    RecupPassComponent,
    NewPassComponent,
    ChangePasswordComponent,
    ProfileComponent,
    OtherProfileComponent,
    FooterComponent,
    RegisterComponent,
    RegisterEntidadeComponent,
    RegistrationChoiceComponent,
    UsersListComponent,
    ActivateComponent,
    SetRecupPassComponent,
    EntitiesListComponent,
    EditProfileComponent,
    AdminPropComponent,
    AdminPropListComponent,
    VolunteeringJobsComponent,
    VolunteeringJobsListComponent,
    ReceivedProposalsComponent,
    ReceivedProposalsListComponent,
    VolunteeringHistoryListComponent,
    ProposalFormComponent,
    VoluntaryFormComponent,
    EntityPropsComponent,
    EntityPropsListComponent,
    VoluntaryProfileComponent,
    InvolvedVoluntariesComponent,
    EditProposalComponent,
    EditVoluntaryComponent,
    ProposalProfileComponent,
    ProposalProfileComponent,
    NotificationComponent,
    UsersComponent,
    OtherProfileVoluntaryListComponent,
    StatsAdminComponent,
    StatsVoluntariadosComponent,
    StatsVoluntariesStateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MultiSelectModule,
    MatBadgeModule,
    MatIconModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC1ZPSnf-cKpBxOnC5eo06-n992Mfjl-Eo',
      libraries: ['places']
    }),
    NgxChartsModule
  ],
  providers: [
    UserGuard,
    EntityGuard,
    AdminGuard,
    UserService,
    EntityService,
    VoluntaryService,
    ProposalService,
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
