import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css']
})
export class ActivateComponent implements OnInit {
  public message1: string;
  public message2: string;

  constructor(
    private userService: UserService, 
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userService.activeAccount(this.router.url.split('/')[2]).subscribe(res => {
      console.log(res);
      if(res.success){
        this.message1 = "A conta está agora verificada!"
        this.message2 = "Confirmas-te a conta associado ao teu email."
      } else {
        this.message1 = "A conta não foi verificada corretamente :("
        this.message2 = "Algo correu mal durante a verificação da sua conta ou já foi corretamente verificada anteriormente."
      }
    });
  }

  activeAccount(){
    this.userService.activeAccount(this.router.url.split('/')[2]);
  }

}
