import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import {MatTableDataSource} from '@angular/material/table';
import { ProposalService } from '../../../shared/proposal/proposal.service'
import { VoluntaryService } from '../../../shared/voluntary/voluntary.service'
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { UserService } from '../../../shared/user.service';
import { Notification } from '../../../shared/notification/notification.model';
import { NotificationService } from '../../../shared/notification/notification.service';
import { VolunteerService } from '../../../shared/volunteer/volunteer.service';

export interface AdminPropListItem {
  id: string;
  email: string;
  userId: string;
  username: string;
  voluntaryName: string;
  submissionDate: string;
}


var DATA: AdminPropListItem[] = [];

@Component({
  selector: 'app-admin-prop-list',
  templateUrl: './admin-prop-list.component.html',
  styleUrls: ['./admin-prop-list.component.css'],
  providers: [ProposalService, UserService, VoluntaryService, NotificationService, VolunteerService]
})
export class AdminPropListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<AdminPropListItem>;
  dataSource = new MatTableDataSource(DATA);
  displayedColumns;

  isAdmin: boolean = false

  constructor(private proposalService: ProposalService, private userService: UserService, private notificationService: NotificationService,private voluntaryService: VoluntaryService, private volunteerService: VolunteerService) { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {}

  ngAfterViewInit() {
    DATA = [];

    this.userService.getUserType().subscribe(userType => {
      if(userType.message == 'Admin'){
        this.isAdmin = true;
      }

      /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
      if(this.isAdmin){
        this.displayedColumns = ['username', 'voluntaryName', 'submissionDate','accept', 'refuse'];
      } else {
        this.displayedColumns = ['username', 'voluntaryName', 'submissionDate'];
      }

      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.proposalService.getProposals().subscribe(res => {
        res.forEach(element => {
          if(element.state == "Pendente"){
            DATA.push({
              id: element._id,
              email: element.voluntary.entity.user.email,
              userId: element.voluntary.entity.user._id,
              username: element.voluntary.entity.user.fullName,
              voluntaryName: element.voluntary.voluntaryName, 
              submissionDate: moment(element.submissionDate).format('l')          
            });
          }
        });
        this.dataSource = new MatTableDataSource(DATA);
        this.table.dataSource = this.dataSource;
      });
    });
  }

  onAccept(row){
    this.proposalService.acceptProposal(row.id).subscribe(res => {
      if(res.success){
        this.voluntaryService.createVoluntaryAfterAccept(res.value).subscribe(response => {
            let newNotification = new Notification();
            newNotification.data = new Date();
            newNotification.text = "A sua proposta de voluntariado foi aceite";
            newNotification.title = "Proposta Aceite!";
            newNotification.user_id = res.value.entity.user._id;
            newNotification.state = "Nova"
            this.notificationService.postNotification(newNotification).subscribe(res => {
              if(res.success) {
              } else {
              }
            });

            this.volunteerService.getVolunteersByInterestArea(response.value.voluntary_areas, response.value._id).subscribe(res => {
              if(response.success){
                Swal.fire({
                  icon: 'success',
                  text: 'Proposta aceite!'
                }).then(function(){
                  window.location.reload();
                });
              } else {
                Swal.fire({
                  icon: 'error',
                  text: response.message.message
                }).then(function(){
                  window.location.reload();
                });
              }
            });
          });
      } else {
        Swal.fire({
          icon: 'error',
          text: 'Algo correu mal! Tente novamente mais tarde!'
        }).then(function(){
          window.location.reload();
        });
      }
    });
  }

  onReject(row){
    Swal.fire({
      title: 'Tem a certeza?',
      text: 'A proposta irá ser removida!',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: "Não",
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim'
    }).then((result) => {
      if (result.value) {
        this.proposalService.rejectProposal(row.id).subscribe(res => {
          if(res.success){
            let newNotification = new Notification();
            newNotification.data = new Date();
            newNotification.text = "A sua proposta de voluntariado foi rejeitada";
            newNotification.title = "Proposta não aceite.";
            newNotification.state = "Nova";
            newNotification.user_id = res.value.entity.user._id;
            this.notificationService.postNotification(newNotification).subscribe(res => {
              if(res.success) {
              } else {
              }
              window.location.reload();
            });
            Swal.fire({
              icon: 'success',
              text: 'Proposta rejeitada!'
            }).then(function(){
              window.location.reload();
            });
          } 
        });
      }
    })
  }
}
