import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { UserService } from '../../shared/user.service';
import {Router} from '@angular/router';
import { User } from 'src/app/shared/user.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  providers: [UserService],
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css']
})
export class EditProfileComponent implements OnInit {
  fullName: String;
  phone: Number;
  address: String;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.userService.getProfile().subscribe(loggedUser => {
      this.fullName = loggedUser.user.fullName;
      this.phone = loggedUser.user.phone;
      this.address = loggedUser.user.address;
    });
  }

  onSubmit(form: NgForm){
    this.userService.editProfile(this.fullName, this.phone, this.address).subscribe(res => {
      this.resetForm(form);
      console.log(res);
      if(res.sucess){
        Swal.fire({
          icon: 'success',
          text: 'Voluntariado editado com sucesso! Irá ser feita uma nova proposta.'
        });
        this.router.navigate(['/profile']);
      }else{
        Swal.fire({
          icon: 'error',
          text: res.message
        });
      }
    });
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      fullName: "",
      email: "",
      phone: 0,
      address: "",
      birthDate: new Date(),
      password: "",
      admin: "",
      photoPath:"",
      suspended: ""
    }
  }

  refresh(): void {
    window.location.reload();
  }
}
