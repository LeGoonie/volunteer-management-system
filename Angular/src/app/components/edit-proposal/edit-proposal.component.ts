import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Entity } from '../../shared/entity/entity.model';
import { EntityService } from '../../shared/entity/entity.service';
import { ProposalService } from '../../shared/proposal/proposal.service';
import { Router } from '@angular/router';
import { Voluntary } from 'src/app/shared/voluntary/voluntary.model';
import Swal from 'sweetalert2';
import { Proposal } from 'src/app/shared/proposal/proposal.model';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-proposal',
  templateUrl: './edit-proposal.component.html',
  providers: [EntityService, ProposalService],
  styleUrls: ['./edit-proposal.component.css', '../../app.component.css']
})
export class EditProposalComponent implements OnInit {
  voluntary: Voluntary;
  entity: Entity;
  entitiesInvolved = "";
  public areas: string[] = [
    "Atividades Académicas (por ex. apoio às matrículas…)",
    "Ambiental (por ex. ações de sensibilização, de limpeza…)",
    "Apoio a Eventos",
    "Informática (por ex. criação de sites, de bases de dados, formação…)",
    "Comunicação (por ex. elaboração de materiais de divulgação, como cartazes…",
    "Cultural (por ex. teatro; música…)",
    "Desporto (por ex. apoio a eventos desportivos, caminhadas…",
    "Educação (por ex. estudo acompanhado, alfabetização…)",
    "Saúde (por ex. rastreios, ações de sensibilização…)",
    "Social (por ex. apoio a idosos, a crianças…)"
  ]
  RGPD: Boolean

  now = new Date();
  minDate = moment({year: this.now.getFullYear(), month: this.now.getMonth(), day: this.now.getDate()}).format('YYYY-MM-DD');

  constructor(private entityService: EntityService, private proposalService: ProposalService, private router: Router) { }

  ngOnInit(): void { 
    this.proposalService.getProposal(window.location.href.split("/")[4]).subscribe(proposal => {
      this.voluntary = proposal.voluntary;

      let counter = 0;
      this.voluntary.entitiesInvolved.forEach(element => {
        if(counter == 0){
          this.entitiesInvolved += element;
        } else {
          this.entitiesInvolved += '; ' + element;
        }
        counter++;
      });
    });
  }

  onSubmit(form: NgForm) {
    this.getSelectedAreas();
    if(this.voluntary.voluntary_areas.length == 0){
      Swal.fire({
        icon: 'error',
        text: "Selecione pelo menos uma area necessária"
      });
    } else {
      this.entityService.getLoggedEntity().subscribe(res => {
        this.entity = res.message;
        if(this.entity){
          var new_voluntary = new Voluntary();
          new_voluntary.voluntaryName = this.voluntary.voluntaryName
          new_voluntary.entity = this.entity
          new_voluntary.description = this.voluntary.description
          new_voluntary.area = this.voluntary.area
          new_voluntary.goals = this.voluntary.goals
          new_voluntary.targetAudience = this.voluntary.targetAudience
          new_voluntary.activityDescription = this.voluntary.activityDescription
          new_voluntary.specificTraining = this.voluntary.specificTraining
          new_voluntary.date = this.voluntary.date
          new_voluntary.entitiesInvolved = this.splitEntitiesInvolvedList();
          new_voluntary.voluntary_areas = this.voluntary.voluntary_areas
          new_voluntary.observations = this.voluntary.observations

          this.proposalService.editProposal(window.location.href.split("/")[4], new_voluntary, Date.now()).subscribe(res => {
            if(res.success) {
              Swal.fire({
                icon: 'success',
                text: 'Proposta editada com sucesso!'
              }).then(function() {
                this.router.navigate(['/myProposals']);
              });              
            } else {
              Swal.fire({
                icon: 'error',
                text: res.message
              });
            }
          });
        } else {
          Swal.fire({
            icon: 'error',
            text: "Algo não correu como o esperado! Parece que não encontramos a entidade referente ao seu utilizador!"
          });
        }
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  showHiddenBox(event){
    var text = document.getElementById(event.target.id+"TextInput");
    if (event.target.checked){
      text.style.display = "block";
    } else {
      (<HTMLInputElement>text).value  = "";
      text.style.display = "none";
    }
  }

  getSelectedAreas(){
    var list = []
    this.areas.forEach(function(err, i, a){
      var checkbox = document.getElementById("area"+i) as HTMLInputElement
      if(checkbox.checked){
        list.push(a[i].toString());
      }
    });
    if((<HTMLInputElement>document.getElementById("check2")).checked){
      list.push((<HTMLInputElement>document.getElementById("check2TextInput")).value);
    }
    this.voluntary.voluntary_areas = list;
  }

  splitEntitiesInvolvedList(){
    let list = []
    this.entitiesInvolved.split('; ').forEach(eName => {
      eName.split(';').forEach(entityName => {
        list.push(entityName)
      });
    });
    return list;
  }
}
