import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVoluntaryComponent } from './edit-voluntary.component';

describe('EditVoluntaryComponent', () => {
  let component: EditVoluntaryComponent;
  let fixture: ComponentFixture<EditVoluntaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVoluntaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVoluntaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
