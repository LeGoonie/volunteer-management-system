import { EntityService } from './../../shared/entity/entity.service';
import { Entity } from './../../shared/entity/entity.model';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';

export interface EntitiesListItem {
  nome: string;
  description: string;
}

const entitiesList = [];

@Component({
  selector: 'app-entities-list',
  templateUrl: './entities-list.component.html',
  styleUrls: ['./entities-list.component.css']
})
export class EntitiesListComponent implements AfterViewInit, OnInit {

  constructor(private entityService: EntityService) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<EntitiesListItem>;
  dataSource = new MatTableDataSource(entitiesList);
  dtTrigger: Subject<Entity> = new Subject();
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['nome', 'description'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    entitiesList.splice(0, entitiesList.length);
      this.entityService.getEntities().subscribe(res => {
        const item = JSON.parse(res['_body']);
        item.forEach(element => {
          entitiesList.push(element);
        });
        this.dtTrigger.next();
      });

    this.dataSource = new MatTableDataSource(entitiesList);
    this.table.dataSource = new MatTableDataSource(entitiesList);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
