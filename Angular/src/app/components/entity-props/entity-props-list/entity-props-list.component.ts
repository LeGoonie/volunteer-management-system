import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import {Router} from '@angular/router';
import { Voluntary } from './../../../shared/voluntary/voluntary.model';
import { ProposalService } from './../../../shared/proposal/proposal.service';
import { EntityService } from './../../../shared/entity/entity.service';
import * as moment from 'moment';
import Swal from 'sweetalert2';

export interface PropsListItem {
  id: string;
  name: string;
  description: string;
  state: string;
  date: string;
}

var DATA: PropsListItem[] = [];

@Component({
  selector: 'app-entity-props-list',
  templateUrl: './entity-props-list.component.html',
  styleUrls: ['./entity-props-list.component.css'],
})

export class EntityPropsListComponent implements AfterViewInit, OnInit {
  constructor(private proposalService: ProposalService, private entityService: EntityService, private router: Router) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<PropsListItem>;
  dataSource = new MatTableDataSource(DATA);
  dtTrigger: Subject<Voluntary> = new Subject();

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'description', 'date', 'approval', 'edit', 'delete'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() : void {}

  ngAfterViewInit() {
    DATA = [];
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    
    this.entityService.getLoggedEntity().subscribe(loggedUserRes => {
      if(loggedUserRes.message._id){
        this.proposalService.getEntityProposals(loggedUserRes.message._id).subscribe(res => {
          const item = JSON.parse(res['_body']);
          item.forEach(element => {
            DATA.push({
              id: element._id,
              name: element.voluntary.voluntaryName,
              description: element.voluntary.description,
              state: element.state,
              date: moment(element.date).locale('pt').format('LL')
            });
          });
          this.dtTrigger.next();
          this.dataSource = new MatTableDataSource(DATA);
          this.table.dataSource = this.dataSource;
        });
      }
    });
  }

  /*ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }*/

  onClickDeleteButton(id, voluntaryName){
    Swal.fire({
      icon: 'warning',
      title: 'Tem a certeza?',
      text: 'Tem a certeza que pretende apagar a proposta para o voluntariado \"' + voluntaryName + '\"',
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonColor: '#fed136',
      cancelButtonColor: '#6a4d8a',
      confirmButtonText: 'Confirmar'
    }).then((result) => {
      if (result.value) {
        this.proposalService.deleteProposal(id).subscribe(res => {
          if(res.success) {
            Swal.fire({
              icon: 'success',
              text: 'Proposta apagada com sucesso!'
            }).then((result) => {
              if (result.value)
                this.ngAfterViewInit()
            });
          } else {
            Swal.fire({
              icon: 'error',
              text: res.message
            });
          }
        })
      }
    });
  }

  onEdit(row){
    this.router.navigate(['/editProposal/' + row.id]);
  }
}