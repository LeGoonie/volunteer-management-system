import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityPropsComponent } from './entity-props.component';

describe('EntityPropsComponent', () => {
  let component: EntityPropsComponent;
  let fixture: ComponentFixture<EntityPropsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntityPropsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityPropsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
