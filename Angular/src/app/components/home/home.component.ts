import { VoluntaryService } from './../../shared/voluntary/voluntary.service';
import { EntityService } from './../../shared/entity/entity.service';
import { VolunteerService } from './../../shared/volunteer/volunteer.service';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css','../../app.component.css', '../../../css/utilAuthForm.css'],
  providers: [UserService, VolunteerService, EntityService, VoluntaryService]
})

export class HomeComponent implements OnInit {
  isAdmin: boolean = false
  isEntity: boolean = false
  isVolunteer: boolean = false
  
  constructor(private volunteerService: VolunteerService, public userService: UserService,
     private entityService: EntityService, private voluntaryService: VoluntaryService) { }
     
  volunteerCounter = 0;
  entityCounter = 0;
  voluntaryCounter = 0;

    getTotalVolunteers(){
      let volunteerList = [];
      this.volunteerService.getVolunteers().subscribe(res => {
        const item = JSON.parse(res['_body']);
        item.forEach(element => {
          volunteerList.push(element);
        });
        this.volunteerCounter = volunteerList.length;
      });
    }

    getTotalEntities(){
      let entityList = [];
      this.entityService.getEntities().subscribe(res => {
        const item = JSON.parse(res['_body']);
        item.forEach(element => {
          entityList.push(element);
        });
        this.entityCounter = entityList.length;
      });
    }

    getTotalVoluntaries(){
      let voluntaryList = [];
      this.voluntaryService.getAllVoluntaries().subscribe(res => {
        const item = JSON.parse(res['_body']);
        item.forEach(element => {
          voluntaryList.push(element);
        });
        this.voluntaryCounter = voluntaryList.length;
      });
    }

  ngOnInit() {
    this.setUserType()
  }

  public setUserType(){
    this.userService.getUserType().subscribe(userType => { 
      if(userType.message == 'Admin'){
        this.isAdmin = true
      } else if(userType.message == 'Entity'){
        this.isEntity = true
      } else {
        this.isVolunteer = true
      }
    })
    this.getTotalVolunteers();
    this.getTotalEntities();
    this.getTotalVoluntaries();
  }

}
