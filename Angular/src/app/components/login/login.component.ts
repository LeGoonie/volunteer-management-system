import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../shared/user.service'
import {Router} from '@angular/router';
import { Body } from '@angular/http/src/body';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.resetForm();
  }

  onSubmit(form: NgForm){
    this.userService.getSuspendedByEmail(form.value.email).subscribe(ress => {
      this.userService.login(form.value).subscribe(res => {
        this.resetForm(form);
        if(ress["_body"] != "true"){
          if(JSON.parse(res["_body"]).success){
            this.userService.storeUserData(JSON.parse(res["_body"]).token, JSON.parse(res["_body"]).user);
            this.router.navigate(['/']);
          } else {
            Swal.fire({
              icon: 'error',
              text: JSON.parse(res["_body"]).message
            });
          }
        } else {
          Swal.fire({
              icon: 'error',
              text: "A sua conta foi suspensa :("
            });
        }

        
            
      });
    }); 
    /*
this.userService.getUserByEmail(form.value.email).subscribe(ress => {
              form.value.user_id = JSON.parse(ress["_body"]);
              this.entityService.postEntity(form.value).subscribe(res2 => {
              this.resetForm(form);
              if(res.sucess){
                this.resetForm(form);
                this.router.navigate(['/login']);
              }else{
                this.router.navigate(['/registerEntidade']);
              }
            });
            
            });
*/   
      
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      fullName: "",
      email: "",
      phone: null,
      address: "",
      birthDate: null,
      password:"",
      admin:"",
      photoPath:"",
      suspended:"",
    }
  }
}
