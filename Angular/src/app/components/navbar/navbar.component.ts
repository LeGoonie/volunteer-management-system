import { Notification } from './../../shared/notification/notification.model';
import { NotificationService } from './../../shared/notification/notification.service';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service'
import { Router } from '@angular/router';
import { HomeComponent} from '../home/home.component';

export interface notificationsListItem {
    _id: string;
    title: string;
    text: string;
    data: Date;
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['../../app.component.css'],
  providers: [UserService]
})

export class NavbarComponent implements OnInit {
  notificationCounter = 0;
  userId = '';
  notificationsList = [];

  constructor(
    public userService: UserService,
    public notificationService: NotificationService,
    private router: Router) { }

  ngOnInit() {
    this.getLoggedUser();
    
    setTimeout(() => {
      this.notificationsCounter();
    }, 500)
   }



   openNotification(id: string){
     this.router.navigate(['/notification/'+id]);
   }

  onLogoutClick(){
    this.userService.logout();
    this.router.navigate(['/']);
    return false;
  }

  getLoggedUser(){
    this.userService.getProfile().subscribe(profile => {
      this.userId = profile.user._id;
    });
    
  }

  notificationsCounter(){
    this.notificationService.getNotificationsOfUser(this.userId).subscribe(res => {
      const item = JSON.parse(res['_body']);
      if(item !== "Deste momento não tem notificações"){
      item.forEach(element => {
        if(element.state === 'Nova'){
          this.notificationsList.push(element);
        }
      });
    }
      this.notificationCounter = this.notificationsList.length;
    });
  }

}