import { Component, OnInit } from '@angular/core';
import { Notification } from "src/app/shared/notification/notification.model";
import { NotificationService } from "src/app/shared/notification/notification.service";
import * as moment from 'moment';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css', '../../app.component.css']
})
export class NotificationComponent implements OnInit {

constructor(private notificationService: NotificationService) { }
title: string;
text: string;
date: String;
state: String;


  ngOnInit(): void {
    var notificationId = window.location.href.split("/")[4];
    
    this.notificationService.getNotification(notificationId).subscribe(notification => {
      this.title = notification.message.title;
      this.text = notification.message.text;
      this.date = moment(notification.data).lang("pt").format('LL');
      this.state = notification.message.state;
      //if(this.state == "Nova") {window.location.reload();}
      this.notificationService.editNotification(notificationId, null, null, "Vista").subscribe(res => {
        console.log(res)
        
      });
    });
    
    
  }

}
