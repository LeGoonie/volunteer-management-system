import { VoluntaryService } from './../../../shared/voluntary/voluntary.service';
import { UserService } from 'src/app/shared/user.service';
import { Router } from '@angular/router';
import { AfterViewInit, Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';

export interface OtherProfileVoluntaryListItem {
  profileLink: string;
  proposalId: string;
  voluntaryId: string;
  voluntaryName: string;
  description: string;
  area: string;
  date: string;
  type: string;
}

var DATA: OtherProfileVoluntaryListItem[] = [];

@Component({
  selector: 'app-other-profile-voluntary-list',
  templateUrl: './other-profile-voluntary-list.component.html',
  styleUrls: ['./other-profile-voluntary-list.component.css']
})

export class OtherProfileVoluntaryListComponent implements AfterViewInit, OnInit {
  @Input() parameters: {id: String, userType: String};
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<OtherProfileVoluntaryListItem>;
  dataSource = new MatTableDataSource(DATA);


  constructor(private voluntaryService: VoluntaryService, private userService: UserService, private router: Router) {  }
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['voluntaryName', 'description', 'area', 'date', 'type'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    DATA = [];
    this.getVolunteeringJobs(this.parameters.id, this.parameters.userType);
    setTimeout(() => {
      this.dataSource = new MatTableDataSource(DATA);
      this.table.dataSource = this.dataSource;
    }, 1500)
  }

  getVolunteeringJobs(userId, userType){
    this.voluntaryService.getUsersVoluntariesById(userId, userType).subscribe(res => {
      res.values.forEach(element => {
        if(element.voluntary){
          DATA.push({
            profileLink: '/proposalProfile/' + element._id,
              proposalId : element._id,
              voluntaryId: element.voluntary._id,
              voluntaryName: element.voluntary.voluntaryName,
              description: element.voluntary.description,
              area: element.voluntary.area,
              date: moment(element.voluntary.date).locale('pt').format('LL'),
              type: "Proposta"
          })
        } else {
          DATA.push({
            profileLink: 'voluntaryProfile/' + element._id,
            proposalId: "",
            voluntaryId: element._id,
            voluntaryName: element.voluntaryName,
            description: element.description,
            area: element.area,
            date: moment(element.date).locale('pt').format('LL'),
            type: "Voluntariado"
          });
        }
      });

    })
  }

  ngAfterViewInit() {
    
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
