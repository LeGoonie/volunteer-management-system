import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/user.model';
import { UserService } from 'src/app/shared/user.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-other-profile',
  templateUrl: './other-profile.component.html',
  styleUrls: ['./other-profile.component.css']
})
export class OtherProfileComponent implements OnInit {
  public user:User;
  userId:string;
  formatedBirthDate:string;
  userType: string
  showPortfolioSection: boolean = false
  tableTitle : string
  counterLabel : string
  childParameters :any
  constructor(private userService:UserService, private router: Router) { }

  ngOnInit(): void {
    this.getData();
  }

getData(){
  this.userId = window.location.href.split("/")[4];

  this.userService.getUserTypeById(this.userId).subscribe(res => { 
    if(res.success){
      this.userType = res.message
      this.childParameters= { id: this.userId, userType: this.userType};
    }
    if(this.userType == 'Volunteer'){
      this.showPortfolioSection = true,
      this.counterLabel = 'Voluntariados Realizados'
      this.tableTitle = 'Meus Voluntariados'
    } else if(this.userType == 'Entity'){
      this.showPortfolioSection = true
      this.counterLabel = 'Propostas/Voluntariados no Sistema'
      this.tableTitle = 'Minhas Propostas/Voluntariados'
    }
    this.userService.getOtherUserProfile(this.userId).subscribe(profile => {
      if(profile.success){
        this.user = profile.user;

        this.formatedBirthDate = moment(this.user.birthDate).lang("pt").format('LL');
      } else {
        Swal.fire({
          icon: 'error',
          text: profile.message
        });
        this.router.navigate(['/']);
        return false
      }
  });

});
}
}