import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';

import { UserService } from '../../shared/user.service';
import { VoluntaryService } from '../../shared/voluntary/voluntary.service';
import { User } from '../../shared/user.model'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UserService, VoluntaryService]
})
export class ProfileComponent implements OnInit {
  public user:User;
  userId:string;
  formatedBirthDate:string;
  images;
  number_voluntaries: string;
  userType: any
  showPortfolioSection: boolean = false
  tableTitle : string
  counterLabel : string

  constructor(private userService:UserService, private router: Router, private voluntaryService: VoluntaryService) {  }

  ngOnInit() {
    this.userService.getUserType().subscribe( res => {
      if(res.success){
        this.userType = res.message
      }
      if(this.userType == 'Volunteer'){
        this.showPortfolioSection = true
        this.counterLabel = "Voluntariados Realizados"
        this.tableTitle = "Meus Voluntariados"
      } else if(this.userType == 'Entity') {
        this.showPortfolioSection = true
        this.counterLabel = "Propostas/Voluntariados no Sistema"
        this.tableTitle = "Minhas Propostas/Voluntariados"
      }
      this.userService.getProfile().subscribe(profile => {
        this.user = profile.user;
        this.userId= profile.user._id;

        this.formatedBirthDate = moment(this.user.birthDate).lang("pt").format('LL');
  
        this.voluntaryService.getUsersVoluntaries(this.userType).subscribe(res => {
          this.number_voluntaries = res.values.length;
          if(this.number_voluntaries == '0') this.showPortfolioSection = false
        });
      },
      err => {
        console.log(err);
        return false;
      });
    });
  }

  onSubmit(form: NgForm){  
    this.userService.deleteUser(this.userId).subscribe(res => {
      console.log(JSON.parse(res["_body"]));
      if(JSON.parse(res["_body"]).success){
        this.userService.storeUserData(JSON.parse(res["_body"]).token, JSON.parse(res["_body"]).user);
      } else {
        console.log(JSON.parse(res["_body"]).message);
      }
      this.userService.logout();
      this.refresh();
    });
  }

  onSelectFile(event){  
    if (event.target.files.length > 0) {
      var file = event.target.files[0];
      this.images = file;

      if(this.images.name.split(".")[1] == "jpeg" || this.images.name.split(".")[1] == "jpg"|| this.images.name.split(".")[1] == "png" || this.images.name.split(".")[1] == "gif"){
        const formData = new FormData();
        formData.append('file', this.images);
        
        this.userService.uploadPhoto(formData).subscribe((res) => {
          if(res.sucess){
            const new_photoPath = "../../../assets/images/" + res.originalName;
            this.userService.changePhoto(new_photoPath).subscribe((res) => {
              if(res.sucess){
                this.refresh();
                return true;
              } else {
                Swal.fire({
                  icon: 'error',
                  text: res.message
                });
                this.router.navigate(['/profile']);
                return false;
              }
            });
          } else {
            Swal.fire({
              icon: 'error',
              text: res.message
            });
            this.router.navigate(['/profile']);
            return false;
          }
        });
      } else {
        Swal.fire({
          icon: 'error',
          text: "Escolha um ficheiro do tipo .jpeg, .png ou .gif!"
        });
      }
    }
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
  }

  refresh(): void {
    window.location.reload();
  }
}