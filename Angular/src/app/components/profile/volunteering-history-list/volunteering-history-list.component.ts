import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { VoluntaryService } from '../../../shared/voluntary/voluntary.service';
import { UserService } from '../../../shared/user.service';
import { ProposalService } from '../../../shared/proposal/proposal.service';
import {Router} from '@angular/router';
import * as moment from 'moment';
import Swal from 'sweetalert2';

export interface VolunteeringHistoryListItem {
  profileLink: string;
  proposalId: string;
  voluntaryId: string;
  voluntaryName: string;
  description: string;
  area: string;
  date: string;
  type: string;
}

// TODO: replace this with real data from your application
var DATA: VolunteeringHistoryListItem[] = [];

@Component({
  selector: 'app-volunteering-history-list',
  templateUrl: './volunteering-history-list.component.html',
  styleUrls: ['./volunteering-history-list.component.css'],
  providers: [VoluntaryService, UserService, ProposalService]
})
export class VolunteeringHistoryListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<VolunteeringHistoryListItem>;
  dataSource = new MatTableDataSource(DATA);

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns;

  isVolunteer = false;

  constructor(private voluntaryService: VoluntaryService, private userService: UserService, private router: Router, private proposalService: ProposalService) {  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {}

  ngAfterViewInit() {
    DATA = [];
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.userService.getUserType().subscribe(userServiceRes => {
      if(userServiceRes.message == "Admin" || userServiceRes.message == "Entity"){
        this.displayedColumns = ['voluntaryName', 'description', 'area', 'date', 'type', 'edit', 'delete'];
      } else {
        this.isVolunteer = true;
        this.displayedColumns = ['voluntaryName', 'description', 'area', 'date', 'type'];
      }


      this.voluntaryService.getUsersVoluntaries(userServiceRes.message).subscribe(res => {
        res.values.forEach(element => {
          if(element.voluntary){
            DATA.push({
              profileLink: '/proposalProfile/' + element._id,
              proposalId : element._id,
              voluntaryId: element.voluntary._id,
              voluntaryName: element.voluntary.voluntaryName,
              description: element.voluntary.description,
              area: element.voluntary.area,
              date: moment(element.voluntary.date).locale('pt').format('LL'),
              type: "Proposta"
            });
          } else {
            DATA.push({
              profileLink: 'voluntaryProfile/' + element._id,
              proposalId: "",
              voluntaryId: element._id,
              voluntaryName: element.voluntaryName,
              description: element.description,
              area: element.area,
              date: moment(element.date).locale('pt').format('LL'),
              type: "Voluntariado"
            });
          }
        });
        this.dataSource = new MatTableDataSource(DATA);
        this.table.dataSource = this.dataSource;
      });
    });
  }

  onClickDeleteButton(row){
    Swal.fire({
      icon: 'warning',
      title: 'Tem a certeza?',
      text: 'Tem a certeza que pretende apagar a proposta para o voluntariado \"' + row.voluntaryName + '\"',
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonColor: '#fed136',
      cancelButtonColor: '#6a4d8a',
      confirmButtonText: 'Confirmar'
    }).then((result) => {
      if (result.value) {
        if(row.type == "Voluntariado"){
          this.voluntaryService.deleteVoluntary(row.voluntaryId).subscribe(resDelete => {
            if(resDelete.success) {
              Swal.fire({
                icon: 'success',
                text: 'Voluntariado apagado com sucesso!'
              }).then((result) => {
                if (result.value)
                  this.ngAfterViewInit()
              });
            } else {
              Swal.fire({
                icon: 'error',
                text: resDelete.message
              });
            }
          });
        } else {
          this.proposalService.deleteProposal(row.proposalId).subscribe(resDelete => {
            if(resDelete.success) {
              Swal.fire({
                icon: 'success',
                text: 'Proposta apagada com sucesso!'
              }).then((result) => {
                if (result.value)
                  this.ngAfterViewInit()
              });
            } else {
              Swal.fire({
                icon: 'error',
                text: resDelete.message
              });
            }
          });
        }       
      }
    });
  }

  onEdit(row){
    if(row.type == "Voluntariado"){
      this.router.navigate(['/editVoluntary/' + row.voluntaryId]);
    } else {
      this.router.navigate(['/editProposal/' + row.proposalId]);
    }
  }
}
