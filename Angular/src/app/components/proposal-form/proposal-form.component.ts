import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { Notification } from '../../shared/notification/notification.model';
import { Entity } from '../../shared/entity/entity.model';
import { EntityService } from '../../shared/entity/entity.service';
import { ProposalService } from '../../shared/proposal/proposal.service';
import { UserService } from '../../shared/user.service';
import { NotificationService } from '../../shared/notification/notification.service';
import { Router } from '@angular/router';
import { Voluntary } from 'src/app/shared/voluntary/voluntary.model';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { MapsAPILoader } from '@agm/core';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';

@Component({
  selector: 'app-proposal-form',
  templateUrl: './proposal-form.component.html',
  providers: [EntityService, ProposalService, UserService, NotificationService],
  styleUrls: ['./proposal-form.component.css', '../../app.component.css']
})

export class ProposalFormComponent implements OnInit {
  voluntaryName: string;
  entity: Entity;
  description: string;
  address:String;
  area: string;
  goals: string;
  targetAudience:string;
  activityDescription:string;
  specificTraining:string;
  date: Date;
  entitiesInvolved: string;
  voluntary_areas: string[];
  observations: string;
  public areas: string[] = [
    "Atividades Académicas (por ex. apoio às matrículas…)",
    "Ambiental (por ex. ações de sensibilização, de limpeza…)",
    "Apoio a Eventos",
    "Informática (por ex. criação de sites, de bases de dados, formação…)",
    "Comunicação (por ex. elaboração de materiais de divulgação, como cartazes…)",
    "Cultural (por ex. teatro; música…)",
    "Desporto (por ex. apoio a eventos desportivos, caminhadas…)",
    "Educação (por ex. estudo acompanhado, alfabetização…)",
    "Saúde (por ex. rastreios, ações de sensibilização…)",
    "Social (por ex. apoio a idosos, a crianças…)"
  ]
  public text: string = 'Select a game'
  RGPD: Boolean

  now = new Date();
  minDate = moment({year: this.now.getFullYear(), month: this.now.getMonth(), day: this.now.getDate()}).format('YYYY-MM-DD');

  @ViewChild('search')
  public searchElementRef: ElementRef;

  public zoom: number;
  public latitude: number;
  public longitude: number;
  public latlongs: any = [];
  public latlong: any = {};
  public searchControl: FormControl;

  constructor(
    private entityService: EntityService,
    private proposalService: ProposalService,
    private userService: UserService,
    private notificationService: NotificationService,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(){
    this.zoom = 8;
      this.latitude = 39.8282;
      this.longitude = -98.5795;

      this.searchControl = new FormControl();
      this.mapsAPILoader.load().then(() => {
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
        autocomplete.addListener('place_changed', () => {
          this.address = autocomplete.getPlace().formatted_address;
        });
      });
  }

  onSubmit(form: NgForm) {
    const usersList = [];
    this.getSelectedAreas()
    if(this.voluntary_areas.length == 0){
      Swal.fire({
        icon: 'error',
        text: "Selecione pelo menos uma area necessária"
      });
    } else {

      this.userService.getUserList().subscribe(res => {
        const item = JSON.parse(res['_body']);
        item.forEach(element => {
          if(element.admin){
            usersList.push(element);
          }
        });
        console.log(usersList);
        usersList.forEach(item => {
          let newNotification = new Notification();
          newNotification.data = new Date();
          newNotification.text = "Recebeu uma nova proposta para voluntariado no sistema";
          newNotification.title = "Nova Proposta!";
          newNotification.user_id = item._id;
          newNotification.state = "Nova"

          this.notificationService.postNotification(newNotification).subscribe(res => {
            if(res.success) {
            } else {
            }
          });
        });
      });


      this.entityService.getLoggedEntity().subscribe(res => {
        this.entity = res.message;
        if(this.entity){
          let voluntary = new Voluntary();
          voluntary.voluntaryName = this.voluntaryName
          voluntary.entity = this.entity
          voluntary.description = this.description
          voluntary.address = this.address
          voluntary.area = this.area
          voluntary.goals = this.goals
          voluntary.targetAudience = this.targetAudience
          voluntary.activityDescription = this.activityDescription
          voluntary.specificTraining = this.specificTraining
          voluntary.date = this.date
          voluntary.entitiesInvolved = this.splitEntitiesInvolvedList()
          voluntary.voluntary_areas = this.voluntary_areas
          voluntary.observations = this.observations
          this.proposalService.postProposal(voluntary).subscribe(res => {
            console.log(res)
            if(res.success) {
              Swal.fire({
                icon: 'success',
                text: 'Proposta enviada com sucesso!'
              });
              this.router.navigate(['/myProposals'])
            } else {
              Swal.fire({
                icon: 'error',
                text: res.message
              });
            }
          });
        } else {
          Swal.fire({
            icon: 'error',
            text: "Algo não correu como o esperado! Parece que não encontramos a entidade referente ao seu utilizador!"
          });
        }
      });
    }
  }

  refresh(): void {
    window.location.reload();
  }

  showHiddenBox(event){
    var text = document.getElementById(event.target.id+"TextInput");
    if (event.target.checked){
      text.style.display = "block";
    } else {
      (<HTMLInputElement>text).value  = "";
      text.style.display = "none";
    }
  }

  getSelectedAreas(){
    var list = []
    this.areas.forEach(function(err, i, a){
      var checkbox = document.getElementById("area"+i) as HTMLInputElement
      if(checkbox.checked){
        list.push(a[i].toString());
      }
    });
    if((<HTMLInputElement>document.getElementById("check2")).checked){
      list.push((<HTMLInputElement>document.getElementById("check2TextInput")).value);
    }
    this.voluntary_areas = list;
  }

  splitEntitiesInvolvedList(){
    let list = []
    if(this.entitiesInvolved)
      this.entitiesInvolved.split('; ').forEach(eName => {
        eName.split(';').forEach(entityName => {
          list.push(entityName)
        });
      });
    return list;
  }
}
