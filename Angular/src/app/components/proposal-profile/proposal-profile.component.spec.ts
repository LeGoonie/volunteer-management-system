import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposalProfileComponent } from './proposal-profile.component';

describe('ProposalProfileComponent', () => {
  let component: ProposalProfileComponent;
  let fixture: ComponentFixture<ProposalProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposalProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposalProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
