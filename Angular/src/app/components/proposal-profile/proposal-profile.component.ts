import { Component, OnInit } from '@angular/core';
import { Proposal } from "src/app/shared/proposal/proposal.model";
import { ProposalService } from "src/app/shared/proposal/proposal.service";
import * as moment from 'moment';

@Component({
  selector: 'app-proposal-profile',
  templateUrl: './proposal-profile.component.html',
  styleUrls: ['./proposal-profile.component.css', '../../app.component.css', '../../../css/utilAuthForm.css']
})
export class ProposalProfileComponent implements OnInit {
  proposal: Proposal;
  areas: [String];
  date: String;

  geocoder: any;
  map: any;

  constructor(private proposalService: ProposalService) { }

  ngOnInit(): void {
    var proposalId = window.location.href.split("/")[4];

    this.proposalService.getProposal(proposalId).subscribe(proposal => {
      this.proposal = proposal;
      this.areas = proposal.voluntary.voluntary_areas;
      this.date = moment(proposal.date).lang("pt").format('LL');

      this.initMap(this.proposal.voluntary.address);
    });
  }

  initMap(address) {
    var map = new google.maps.Map(document.getElementById("map"), {
      zoom: 15,
      center: { lat: -34.397, lng: 150.644 }
    });
    var geocoder = new google.maps.Geocoder();
  
    this.geocodeAddress(geocoder, map, address);
  }

  geocodeAddress(geocoder, resultsMap, address) {
    geocoder.geocode({ address: address }, function(results, status) {
      if (status === "OK") {
        console.log(results);
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
}
