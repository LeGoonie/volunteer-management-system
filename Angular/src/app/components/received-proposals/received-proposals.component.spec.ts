import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivedProposalsComponent } from './received-proposals.component';

describe('ReceivedProposalsComponent', () => {
  let component: ReceivedProposalsComponent;
  let fixture: ComponentFixture<ReceivedProposalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceivedProposalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivedProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
