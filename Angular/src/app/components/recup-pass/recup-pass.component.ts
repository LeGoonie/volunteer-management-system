import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../shared/user.service'
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-recup-pass',
  templateUrl: './recup-pass.component.html',
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css'],
  providers: [UserService]
})
export class RecupPassComponent implements OnInit {
  email: String

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.resetForm();
  }

  recoveryPasswordSubmit(form: NgForm){
    this.userService.recoveryPassword(form.value).subscribe((res) => {
      this.resetForm(form);
      console.log(res.message);
      if(res.sucess){
        Swal.fire({
          icon: 'success',
          text: res.message
        });
        this.router.navigate(['/']);
        return true;
      } else {
        Swal.fire({
          icon: 'error',
          text: res.message
        });
        this.router.navigate(['/recupPass']);
        return false;
      }
    });

    this.resetForm(form);
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      fullName: "",
      email: "",
      phone: null,
      address: "",
      birthDate: null,
      password:"",
      admin:"",
      photoPath:"",
      suspended:"",
    }
  }

}
