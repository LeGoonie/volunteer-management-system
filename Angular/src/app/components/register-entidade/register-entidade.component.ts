import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import {Http, Headers} from '@angular/http';
import { UserService } from '../../shared/user.service';
import { User } from '../../shared/user.model';
import { EntityService } from '../../shared/entity/entity.service';
import { Entity } from '../../shared/entity/entity.model';
import { Body } from '@angular/http/src/body';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { MapsAPILoader } from '@agm/core';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';

@Component({
  selector: 'app-register',
  templateUrl: './register-entidade.component.html',
  providers: [UserService, EntityService],
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css', '../../app.component.css', './register-entidade.component.css']
})

export class RegisterEntidadeComponent implements OnInit {
  fullName: String;
  email: String;
  phone: Number;
  address: String;
  birthDate: Date;
  password: String;
  passwordConfirmed: String;
  admin: String;
  user: User;
  nome: String;
  description: String;
  now = new Date();
  minDate = moment({year: this.now.getFullYear() - 100, month: this.now.getMonth(), day: this.now.getDay()}).format('YYYY-MM-DD');
  maxDate = moment({year: this.now.getFullYear() - 16, month: this.now.getMonth(), day: this.now.getDay()}).format('YYYY-MM-DD');

  @ViewChild('search')
  public searchElementRef: ElementRef;

  public zoom: number;
  public latitude: number;
  public longitude: number;
  public latlongs: any = [];
  public latlong: any = {};
  public searchControl: FormControl;

  constructor(
    private userService: UserService, 
    private entityService: EntityService, 
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
    ) { }

  ngOnInit() {
    this.resetForm();
  }

  ngAfterViewInit(){
    this.zoom = 8;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    this.searchControl = new FormControl();
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.address = autocomplete.getPlace().formatted_address;
      });
    });
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      fullName: "",
      email: "",
      phone: 0,
      address: "",
      birthDate: new Date(),
      password: "",
      admin: "",
      photoPath:"",
      suspended:"",
    }
    this.entityService.selectedEntity = {
      _id: "",
      user: new User(),
      nome: "",
      description: "",
    }
  }


  onSubmit(form: NgForm) {    
    const user = {
      _id: "",
      fullName: this.fullName,
      email: this.email,
      phone: this.phone,
      address: this.address,
      birthDate: this.birthDate,
      password: this.password,
      admin: this.admin,
      suspended: "false",
      photoPath: "",
      passConfirmed: this.passwordConfirmed
    }

    const entity = {
      nome: this.nome,
      description: this.description,
    }

    if(this.password != this.passwordConfirmed){
      Swal.fire({
        icon: 'error',
        text: "A sua password não combina com a sua confirmação de password"
      });
    } else if (this.phone.toString().length != 9) {
      Swal.fire({
        icon: 'error',
        text: "O numero de telefone deve ter 9 digitos"
      });
    }

    //Registar utilizador
    this.userService.register(user).subscribe(res => {       
        if(res.sucess){        
            this.userService.getUserIdByEmail(form.value.email).subscribe(ress => {             
              //form.value.user = { fullName: this.fullName, email: this.email, phone: this.phone, address: this.address, birthDate: this.birthDate, admin: this.admin, photoPath: "../../../assets/images/default_profile.png", active: false, suspended: false}
              form.value.user_id = JSON.parse(ress["_body"]);
                this.entityService.postEntity(form.value).subscribe(res2 => {
                this.resetForm(form);
                if(res.sucess){
                  this.resetForm(form);
                  this.router.navigate(['/login']);
                }else{
                  this.router.navigate(['/registerEntidade']);
                }
              });
            });
            
        }else{
          this.router.navigate(['/registerEntidade']);
        }
    });
  }
}
