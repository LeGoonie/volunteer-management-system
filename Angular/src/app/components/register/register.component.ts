import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import {Http, Headers} from '@angular/http';
import { UserService } from '../../shared/user.service';
import { User } from '../../shared/user.model';
import { VolunteerService } from '../../shared/volunteer/volunteer.service';
import { Volunteer } from '../../shared/volunteer/volunteer.model';
import { Body } from '@angular/http/src/body';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { MapsAPILoader } from '@agm/core';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';

declare var $:any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [UserService, VolunteerService],
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css', './register.component.css', '../../app.component.css']
})

export class RegisterComponent implements OnInit {
  selectCursos = [];

  fullName: String;
  email: String;
  phone: Number;
  address: String;
  birthDate: Date;
  password: String;
  passwordConfirmed: String;
  admin: String;
  interestAreas: string[];
  typeOfMember: String;
  schoolService: String;
  course: String;
  observations: String;
  reasonsVolunteer: string[];
  now = new Date();
  minDate = moment({year: this.now.getFullYear() - 100, month: this.now.getMonth(), day: this.now.getDate()}).format('YYYY-MM-DD');
  maxDate = moment({year: this.now.getFullYear() - 16, month: this.now.getMonth(), day: this.now.getDate()}).format('YYYY-MM-DD');
  public areas: string[] = [
    "Atividades Académicas (por ex. apoio às matrículas…)",
    "Ambiental (por ex. ações de sensibilização, de limpeza…)",
    "Apoio a Eventos",
    "Informática (por ex. criação de sites, de bases de dados, formação…)",
    "Comunicação (por ex. divulgação nas Escolas Secundárias/Profissionais, Futurália…)",
    "Cultural (por ex. teatro; música…)",
    "Desporto (por ex. apoio a eventos desportivos, caminhadas…)",
    "Educação (por ex. estudo acompanhado, alfabetização…)",
    "Saúde (por ex. rastreios, ações de sensibilização…)",
    "Social (por ex. apoio a idosos, a crianças, Banco Alimentar…)"
  ];
  public membros: string[] = [
    "Estudante",
    "Diplomado",
    "Docente",
    "Não Docente",
    "Bolseiro",
    "Aposentado"
  ];
  public razoes: string[] = [
    "Pelo convívio social",
    "Porque pode ser vantajoso para o futuro profissional",
    "Pela possibilidade de integração social",
    "Para ter novas experiências",
    "Porque gosto de ajudar os outros",
    "Porque fui incentivado(a) por outras pessoas",
    "Porque conheço pessoas que já realizaram atividades de voluntariado no IPS",
    "Para me sentir útil",
    "Para ocupar tempo livre"
  ];
  public schools: string[] = [
    "Escola Superior de Tecnologia de Setúbal (ESTS)",
    "Escola Superior de Saúde (ESS)",
    "Escola Superior de Educação (ESE)",
    "Escola Superior de Ciências Empresariais (ESCE)",
    "Não sou membro do IPS"
  ]
  public escolasServicos: string[] = [
    "Escola Superior de Tecnologia de Setúbal",
    "Escola Superior de Educação",
    "Escola Superior de Ciências Empresariais",
    "Escola Superior de Tecnologia do barreiro",
    "Escola Superior de Saúde"
  ]

  public est: string[] = [
    "Engenharia de Automação, Controlo e Instrumentação",
    "Engenharia Electrotécnica e de Computadores",
    "Engenharia Informática",
    "Engenharia Mecânica",
    "Tecnologia Biomédica",
    "Tecnologias de Energia",
    "Tecnologias e Gestão Industrial",
    "Tecnologias do Ambiente e do Mar"
  ]

  public ese: string[] = [
    "Licenciatura em Animação e Intervenção Sociocultural",
    "Licenciatura em Comunicação Social",
    "Licenciatura em Desporto",
    "Licenciatura em Educação Básica",
    "Licenciatura em Tradução e Interpretação de Língua Gestual Portuguesa"
  ]

  public esce: string[] = [
    "Contabilidade e Finanças",
    "Contabilidade e Finanças Noturno",
    "Gestão da Distribuição e da Logística",
    "Gestão da Distribuição e da Logística Pós-Laboral",
    "Gestão de Recursos Humanos",
    "Gestão de Recursos Humanos Pós-Laboral",
    "Gestão de Sistemas de Informação",
    "Marketing"
  ]

  public estb: string[] = [
    "Bioinformática",
    "Biotecnologia",
    "Engenharia Civil",
    "Engenharia Química",
    "Gestão da Construção",
    "Tecnologias do Petróleo"
  ]

  public ess: string[] = [
    "Acupuntura",
    "Enfermagem",
    "Fisioterapia",
    "Terapia da Fala"
  ]
  RGPD: Boolean

  @ViewChild('search')
  public searchElementRef: ElementRef;

  public zoom: number;
  public latitude: number;
  public longitude: number;
  public latlongs: any = [];
  public latlong: any = {};
  public searchControl: FormControl;

  constructor(
    private userService: UserService,
    private volunteerService: VolunteerService,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) 
    {}

  ngOnInit() {
    this.resetForm();
  }

  ngAfterViewInit(){
    this.zoom = 8;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    this.searchControl = new FormControl();
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.address = autocomplete.getPlace().formatted_address;
      });
    });
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      fullName: "",
      email: "",
      phone: 0,
      address: "",
      birthDate: new Date(),
      password: "",
      admin: "",
      photoPath:"",
      suspended:"",
    }

    this.volunteerService.selectedVolunteer = {
      _id: "",
      user: new User(),
      typeOfMember: "",
      schoolService: "",
      course: "",
      observations: "",
      interestAreas: [],
      admin: "",
      reasonsVolunteer: [],

    }
  }

  onSubmit(form: NgForm) {
    const user = {
      _id: "",
      fullName: this.fullName,
      email: this.email,
      phone: this.phone,
      address: this.address,
      birthDate: this.birthDate,
      password: this.password,
      admin: this.admin,
      suspended: "false",
      photoPath: "",
      passConfirmed: this.passwordConfirmed
    }

    const volunteer = {
      typeOfMember: this.typeOfMember,
      schoolService: this.schoolService,
      course: this.course,
      interestAreas: this.interestAreas,
      reasonsVolunteer: this.reasonsVolunteer,
      observations: this.observations,
    }

    this.getSelectedAreas();
    this.getSelectedRazoes();
    this.getSelectedMembro();
    
    if(this.password != this.passwordConfirmed){
      Swal.fire({
        icon: 'error',
        text: "A sua password não combina com a sua confirmação de password"
      });
    } else if(this.phone.toString().length != 9){
      Swal.fire({
        icon: 'error',
        text: "O numero de telefone deve ter 9 digitos"
      });
    }else if(this.interestAreas.length == 0 ){
      Swal.fire({
        icon: 'error',
        text: "Selecione pelo menos uma area necessária"
      });
    } else if (this.reasonsVolunteer.length == 0 ){
      Swal.fire({
        icon: 'error',
        text: "Selecione pelo menos uma razão para querer ser voluntário"
      });
    } else {
    //Registar utilizador
    this.userService.register(user).subscribe(res => {
      if(res.sucess){
        this.userService.getUserIdByEmail(form.value.email).subscribe(ress => {
          if(res.sucess){
            form.value.user_id = JSON.parse(ress["_body"]);
          
          this.volunteerService.postVolunteer(form.value).subscribe(res2 => {
            this.resetForm(form);
              if(res2.sucess){
                this.resetForm(form);
                if(res2.sucess){
                  this.resetForm(form);
                  Swal.fire({
                    icon: 'success',
                    text: res.message
                  });
                  this.router.navigate(['/login']);
                }else{
                  Swal.fire({
                    icon: 'error',
                    text: res.message
                  });
                  this.refresh();
                  this.router.navigate(['/register']);
                }
              }
            });
            } else {
              Swal.fire({
                  icon: 'error',
                  text: res.message
                });
                this.refresh();
                this.router.navigate(['/register']);
            }         
          });
        }else{
          this.router.navigate(['/register']);
        }
        
      });
    
    }
  }

  refresh(): void {
    window.location.reload();
  }

  showHiddenBox(event){
    let elementId = event.target.id
    let text = <HTMLInputElement>document.getElementById(elementId+"TextInput");
    if(elementId == "schoolSelector"){
      let selectBox = <HTMLInputElement>document.getElementById(elementId);
      if(selectBox.value == "Não sou membro do IPS"){
        text.style.display = "block";
      } else {
        text.value  = "";
        text.style.display = "none";
      }
    } else {
      if (event.target.checked){
        text.style.display = "block";
      } else {
        text.value  = "";
        text.style.display = "none";
      }
    }
  }

  getSelectedAreas(){
  var list = []
  this.areas.forEach(function(err, i, a){
    var checkbox = document.getElementById("area"+i) as HTMLInputElement
    if(checkbox.checked){
      list.push(a[i].toString());
    }
    });
    if((<HTMLInputElement>document.getElementById("check2")).checked){
      list.push((<HTMLInputElement>document.getElementById("check2TextInput")).value);
    }
    this.interestAreas = list;
  }

  getSelectedRazoes(){
  var list = [];
  this.razoes.forEach(function(err, i, a){
    var checkbox = document.getElementById("razao"+i) as HTMLInputElement
    if(checkbox.checked){
      list.push(a[i].toString());
    }
    });
    if((<HTMLInputElement>document.getElementById("check3")).checked){
      list.push((<HTMLInputElement>document.getElementById("check3TextInput")).value);
    }
    this.reasonsVolunteer = list;
  }

  getSelectedMembro(){
    var opcao;
    this.membros.forEach(function(err, i, a){
      var radiobutton = document.getElementById("membro"+i) as HTMLInputElement
      if(radiobutton.checked){
        opcao = (a[i].toString());
      }
    });
    console.log(opcao)
    this.typeOfMember = opcao;
  }

  getSelectedEscolaServico(){
    var opcao;
    var j;
    this.escolasServicos.forEach(function(err, i, a){
      var radiobutton = document.getElementById("escolasservico"+i) as HTMLInputElement
      if(radiobutton.checked){
        opcao = (a[i].toString());
        j = i;
      }
    });
    let arr = [];
    this.getCursos(j);
    this.schoolService = opcao;
  }

  getSelectedCurso(){
    var opcao;
    var j;
    this.escolasServicos.forEach(function(err, i, a){
      var radiobutton = document.getElementById("escolasservico"+i) as HTMLInputElement
      if(radiobutton.checked){
        opcao = (a[i].toString());
        j = i;
      }
    });
    let arr = [];
    this.getCursos(j);
    this.schoolService = opcao;
  }

  getCursos(opcao){
    if(opcao == 0){
      this.selectCursos = this.est;
    } else if (opcao == 1){
      this.selectCursos = this.ese;
    } else if (opcao == 2){
      this.selectCursos = this.esce;
    } else if (opcao == 3){
      this.selectCursos = this.estb;
    } else if (opcao == 4){
      this.selectCursos = this.ess;
    }
  }  
}
