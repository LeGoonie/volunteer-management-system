import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registration-choice',
  templateUrl: './registration-choice.component.html',
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css']
})
export class RegistrationChoiceComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
