import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../shared/user.service'
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-set-recup-pass',
  templateUrl: './set-recup-pass.component.html',
  styleUrls: ['../../../css/mainAuthForm.css', '../../../css/utilAuthForm.css'],
  providers: [UserService]
})
export class SetRecupPassComponent implements OnInit {
  new_password: String;
  new_password_v2: String;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.resetForm();
  }

  recoveryPasswordSubmit(form: NgForm){
    let token = this.router.url.split('/')[2]
    this.userService.recoveryPasswordConfirm(token, form.value).subscribe(res => {
      this.resetForm(form);
      if(res.success){
        Swal.fire({
          icon: 'success',
          text: res.message
        });
        this.router.navigate(['/']);
        return true;
      } else {
        Swal.fire({
          icon: 'error',
          text: res.message
        });
        this.router.navigate(['/recupPassConfirm' + `/${token}`]);
        return false;
      }
    });

    this.resetForm(form);
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      fullName: "",
      email: "",
      phone: null,
      address: "",
      birthDate: null,
      password:"",
      admin:"",
      photoPath:"",
      suspended:"",
    }
  }
}
