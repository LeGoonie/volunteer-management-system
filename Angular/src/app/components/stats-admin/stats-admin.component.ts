import { StatisticsService } from './../../shared/statistics/statistics.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stats-admin',
  templateUrl: './stats-admin.component.html',
  styleUrls: ['../../app.component.css', '../../../css/utilAuthForm.css'],
  providers: [StatisticsService]
})
export class StatsAdminComponent implements OnInit {  

  constructor(private statsService: StatisticsService) {
  }
  averageVolunteersByJob = 0;

  ngOnInit() {
    this.getAverageVolunteersByJob();
  }

    getAverageVolunteersByJob(){
      this.statsService.getAverageVolunteerPerVoluntary().subscribe(res => {
        this.averageVolunteersByJob = res.toFixed(1);
      });
    }

}