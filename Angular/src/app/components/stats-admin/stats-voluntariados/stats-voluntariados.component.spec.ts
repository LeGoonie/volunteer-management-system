import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsVoluntariadosComponent } from './stats-voluntariados.component';

describe('StatsVoluntariadosComponent', () => {
  let component: StatsVoluntariadosComponent;
  let fixture: ComponentFixture<StatsVoluntariadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsVoluntariadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsVoluntariadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
