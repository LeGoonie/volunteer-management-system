import { StatisticsService } from './../../../shared/statistics/statistics.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stats-voluntariados',
  templateUrl: './stats-voluntariados.component.html',
  styleUrls: ['../../../app.component.css'],
  providers: [StatisticsService]
})
export class StatsVoluntariadosComponent implements OnInit {
  
  public data = []

   view: any[] = [1000, 350];
    gradient = false;
    showLegend = true;
    legendPosition: string = 'below';
    legendTitle: string = "Legenda";
    showLabels: boolean = true;
    isDoughnut: boolean = true;
     colorScheme = {
      domain: ['#f1d204', '#f18b6d', '#545454', '#6a4d8a', '#222f36']};

      
  constructor(private statsService: StatisticsService) { }

  ngOnInit(): void {
    this.getAreasPerVoluntary();
  }

  getAreasPerVoluntary(){
    this.statsService.getAreasPerVoluntary().subscribe(res => {
      for(var aux in res){
        this.data.push({name:aux, value:res[aux]})
      }
    });
    
  }

}
