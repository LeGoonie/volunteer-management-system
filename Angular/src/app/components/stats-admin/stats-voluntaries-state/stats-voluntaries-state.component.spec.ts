import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatsVoluntariesStateComponent } from './stats-voluntaries-state.component';

describe('StatsVoluntariesStateComponent', () => {
  let component: StatsVoluntariesStateComponent;
  let fixture: ComponentFixture<StatsVoluntariesStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatsVoluntariesStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatsVoluntariesStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
