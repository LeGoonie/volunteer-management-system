import { Component, OnInit } from '@angular/core';
import { StatisticsService } from './../../../shared/statistics/statistics.service';

@Component({
  selector: 'app-stats-voluntaries-state',
  templateUrl: './stats-voluntaries-state.component.html',
  styleUrls: ['../../../app.component.css'],
  providers: [StatisticsService]
})
export class StatsVoluntariesStateComponent implements OnInit {

  public data = []

  view: any[] = [1000, 350];
    gradient = false;
    showLegend = true;
    legendPosition: string = 'below';
    legendTitle: string = "Legenda"
    showLabels: boolean = true;
    isDoughnut: boolean = true;
     colorScheme = {
      domain: ['#f1d204', '#f18b6d', '#545454', '#6a4d8a', '#222f36']};

  constructor(private statsService: StatisticsService) { }

  ngOnInit(): void {
    this.getAcceptedRejectedPending();
  }

  getAcceptedRejectedPending(){
    this.statsService.getAcceptedRejectedPending().subscribe(res => {
      var arr = res.split(',');
      let aux = parseInt(arr[0]) + parseInt(arr[1]) + parseInt(arr[2]);
      this.data.push({name:"Aceites", value:(parseInt(arr[0])/aux)*100})
      this.data.push({name:"Pendentes", value:(parseInt(arr[1])/aux)*100})
      this.data.push({name:"Rejeitados", value:(parseInt(arr[2])/aux)*100})
    })
  }

}
