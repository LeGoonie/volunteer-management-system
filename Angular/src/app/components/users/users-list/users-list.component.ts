import { User } from 'src/app/shared/user.model';
import { UserService } from 'src/app/shared/user.service';
import { AfterViewInit, Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import {Router} from "@angular/router"

export interface UsersListItem {
  fullName: string;
  email: string;
  phone: number;
}

const usersList = [];


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, AfterViewInit {

  constructor(private userService: UserService, private router: Router) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<UsersListItem>;
  dataSource = new MatTableDataSource(usersList);
  isAdmin: boolean = false;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'email', 'phone', 'open', 'suspend'];


  setUserType(){
    this.userService.getUserType().subscribe(userType => { 
      if(userType.message == "Admin"){
        this.isAdmin = true;
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openProfile(id: string){
    this.router.navigate(['/profile', id]);
  }

  suspendProfile(id: string){
    let obj = this;
    usersList.forEach(function(aux){
      if(aux._id == id){
        obj.userService.putUserSuspended(aux).subscribe(ress => {
          console.log(ress);
        });
      }
    });
  }

  ngOnInit(){
    usersList.splice(0, usersList.length);
    this.setUserType();
    
    this.userService.getUserList().subscribe(res => {
      const item = JSON.parse(res['_body']);
      item.forEach(element => {
        usersList.push(element);
      });
    });
    setTimeout(() => {
      this.dataSource = new MatTableDataSource(usersList);
      this.table.dataSource = this.dataSource;
    }, 1500)
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
