/* import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoluntaryFormComponent } from './voluntary-form.component';

describe('VoluntaryFormComponent', () => {
  let component: VoluntaryFormComponent;
  let fixture: ComponentFixture<VoluntaryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoluntaryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoluntaryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
 */

import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { VoluntaryService } from '../../shared/voluntary/voluntary.service';
import { Voluntary } from '../../shared/voluntary/voluntary.model';

  /*it(
    'should perform login correctly',
    fakeAsync(
      inject(
        [VoluntaryService, HttpTestingController],
        (voluntaryService: VoluntaryService, backend: HttpTestingController) => {

          // Set up
          const url = 'https://example.com/login';
          const responseObject = {
            success: true,
            message: 'login was successful'
          };
          const voluntary = new Voluntary('test@example.com', 'testpassword');
          let response = null;
          // End Setup

          voluntaryService.onLogin(voluntary).subscribe(
            (receivedResponse: any) => {
              response = receivedResponse;
            },
            (error: any) => {}
          );

          const requestWrapper = backend.expectOne({url: 'https://example.com/login'});
          requestWrapper.flush(responseObject);

          tick();

          expect(requestWrapper.request.method).toEqual('POST');
          expect(response.body).toEqual(responseObject);
          expect(response.status).toBe(200);
        }
      )
    )
  );

  it(
    'should fail login correctly',
    fakeAsync(
      inject(
        [AuthService, HttpTestingController],
        (authService: AuthService, backend: HttpTestingController) => {

          // Set up
          const url = 'https://example.com/login';
          const responseObject = {
            success: false,
            message: 'email and password combination is wrong'
          };
          const user = new User('test@example.com', 'wrongPassword');
          let response = null;
          // End Setup

          authService.onLogin(user).subscribe(
            (receivedResponse: any) => {
              response = receivedResponse;
            },
            (error: any) => {}
          );

          const requestWrapper = backend.expectOne({url: 'https://example.com/login'});
          requestWrapper.flush(responseObject);

          tick();

          expect(requestWrapper.request.method).toEqual('POST');
          expect(response.body).toEqual(responseObject);
          expect(response.status).toBe(200);
        }
      )
    )
  );*/
