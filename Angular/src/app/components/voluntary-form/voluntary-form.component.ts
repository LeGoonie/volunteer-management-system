import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { User } from 'src/app/shared/user.model';
import {Router} from '@angular/router';
import { NgForm, FormControl } from '@angular/forms';
import { Volunteer } from 'src/app/shared/volunteer/volunteer.model';
import { Entity } from 'src/app/shared/entity/entity.model';
import { VolunteerService } from '../../shared/volunteer/volunteer.service';
import { VoluntaryService } from '../../shared/voluntary/voluntary.service';
import { EntityService } from '../../shared/entity/entity.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { MapsAPILoader } from '@agm/core';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';

@Component({
  selector: 'app-voluntary-form',
  templateUrl: './voluntary-form.component.html',
  styleUrls: ['./voluntary-form.component.css', '../../app.component.css'],
  providers: [VolunteerService, VoluntaryService, EntityService]
})
export class VoluntaryFormComponent implements OnInit {
  voluntaryName: String;
  entityName: String;
  entity: Entity;
  description: string;
  address:String;
  area: String;
  goals: string;
  targetAudience:string;
  activityDescription:string;
  specificTraining:string;
  date: Date;
  entitiesInvolved: String;
  voluntary_areas: string[];
  observations: string;
  public areas:String[] = [
    "Atividades Académicas (por ex. apoio às matrículas…)",
    "Ambiental (por ex. ações de sensibilização, de limpeza…)",
    "Apoio a Eventos",
    "Informática (por ex. criação de sites, de bases de dados, formação…)",
    "Comunicação (por ex. elaboração de materiais de divulgação, como cartazes…",
    "Cultural (por ex. teatro; música…)",
    "Desporto (por ex. apoio a eventos desportivos, caminhadas…",
    "Educação (por ex. estudo acompanhado, alfabetização…)",
    "Saúde (por ex. rastreios, ações de sensibilização…)",
    "Social (por ex. apoio a idosos, a crianças…)"
  ]
  RGPD: Boolean;

  @ViewChild('search')
  public searchElementRef: ElementRef;

  public zoom: number;
  public latitude: number;
  public longitude: number;
  public latlongs: any = [];
  public latlong: any = {};
  public searchControl: FormControl;

  now = new Date();
  minDate = moment({year: this.now.getFullYear(), month: this.now.getMonth(), day: this.now.getDate()}).format('YYYY-MM-DD');

  constructor(private voluntaryService: VoluntaryService, 
    private entityService: EntityService, 
    private volunteerService: VolunteerService, 
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) 
    {}

  ngOnInit(): void {}

  ngAfterViewInit(){
    this.zoom = 8;
      this.latitude = 39.8282;
      this.longitude = -98.5795;

      this.searchControl = new FormControl();
      this.mapsAPILoader.load().then(() => {
        const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
        autocomplete.addListener('place_changed', () => {
          this.address = autocomplete.getPlace().formatted_address;
        });
      });
  }

  onSubmit(form: NgForm) {
    var entities_names = [];
    var entities_involved = [];
    this.getSelectedAreas();

    this.entityService.getEntityByName(this.entityName).subscribe(res => {
      if(res.success){
        this.voluntaryService.createVoluntary(this.voluntaryName, res.value, this.description, this.address, this.area, this.goals, this.targetAudience, this.activityDescription, 
          this.specificTraining, this.voluntary_areas, this.observations, this.entitiesInvolved).subscribe(response => {
            if(response.success){
              this.volunteerService.getVolunteersByInterestArea(this.voluntary_areas, response.value._id).subscribe(res => {
                this.router.navigate(['/voluntaryProfile/' + response.value._id]);
              });
            } else {
              Swal.fire({
                icon: 'error',
                text: response.message
              }).then(function(){
                form.reset();
              });
            }
          });
      } else {
        Swal.fire({
          icon: 'error',
          text: 'Algo correu mal!'
        })
      }
    });
  }

  selectItem(area){
    this.area = area;
  }

  showHiddenBox(event){
    var text = document.getElementById(event.target.id+"TextInput");
    if (event.target.checked){
      text.style.display = "block";
    } else {
      text.style.display = "none";
    }
  }

  getSelectedAreas(){
    var list = []
    this.areas.forEach(function(err, i, a){
      var checkbox = document.getElementById("area"+i) as HTMLInputElement
      if(checkbox.checked){
        list.push(a[i].toString());
      }
    });
    this.voluntary_areas = list;
  }
}