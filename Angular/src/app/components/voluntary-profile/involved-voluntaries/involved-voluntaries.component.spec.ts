import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvolvedVoluntariesComponent } from './involved-voluntaries.component';

describe('InvolvedVoluntariesComponent', () => {
  let component: InvolvedVoluntariesComponent;
  let fixture: ComponentFixture<InvolvedVoluntariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvolvedVoluntariesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvolvedVoluntariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
