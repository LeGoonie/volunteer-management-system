import { AfterViewInit, Component, OnInit, ViewChild, ɵɵcontainerRefreshEnd } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { VoluntaryService } from '../../../shared/voluntary/voluntary.service'
import { UserService } from '../../../shared/user.service';
import { Voluntary } from 'src/app/shared/voluntary/voluntary.model';
import * as moment from 'moment';
import { User } from 'src/app/shared/user.model';
import { Notification } from '../../../shared/notification/notification.model';
import { NotificationService } from '../../../shared/notification/notification.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface Participants {
  id: string;
  name: string;
  email: string;
  typeOfMember: string;
  course: string;
}

// TODO: replace this with real data from your application
var DATA: Participants[] = [];

@Component({
  selector: 'app-involved-voluntaries',
  templateUrl: './involved-voluntaries.component.html',
  styleUrls: ['./involved-voluntaries.component.css'],
  providers: [VoluntaryService, UserService, NotificationService]
})
export class InvolvedVoluntariesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<Participants>;
  dataSource = new MatTableDataSource(DATA);

  
  isAdmin: boolean = false;
  isVolunteer: boolean = false;
  isInvolved: boolean = false;
  voluntaryId: String;
  userId: string;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'email', 'typeOfMember', 'course'];

  constructor(private voluntaryService: VoluntaryService, private userService: UserService, private notificationService: NotificationService, private router: Router) { }

  ngOnInit(): void {}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngAfterViewInit() {
    DATA = [];
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.voluntaryId = window.location.href.split("/")[4];

    this.voluntaryService.getVoluntary(this.voluntaryId).subscribe(voluntary => {
      voluntary.volunteers.forEach(element => {
        DATA.push({
          id: element.user._id,
          name: element.user.fullName,
          email: element.user.email,
          typeOfMember: element.typeOfMember, 
          course: element.course
        });
      });
      this.dataSource = new MatTableDataSource(DATA);
      this.table.dataSource = this.dataSource;
      this.setUserType();
    });
  }

  setUserType(){
    this.userService.getUserType().subscribe(userType => { 
      if(userType.message == 'Volunteer'){
        this.isVolunteer = true;
        this.voluntaryService.isVolunteerInvolved(this.voluntaryId).subscribe(res => {
          this.isInvolved = res.success;
        });
      } else if(userType.message == "Admin"){
        this.isAdmin = true;
      }
    });
  }

  delete(){
    Swal.fire({
      title: 'Tem a certeza?',
      text: 'O voluntariado irá ser removido!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim'
    }).then((result) => {
      if(result.value){
        this.voluntaryService.deleteVoluntary(this.voluntaryId).subscribe(res => {
          if(res.success){
            Swal.fire({
              icon: 'success',
              text: 'Voluntariado Apagado!'
            }).then((result) =>{
                if(result.value){
                  this.router.navigate(['/']);
                } 
                
            });
          }
        })
      }
    })
  }

  enroll(){ 
    this.isInvolved = true;
    this.voluntaryService.getVoluntary(this.voluntaryId).subscribe(res => {this.userId = res.entity.user._id});
    console.log("user id:" + this.userId);
    this.userService.getUserType().subscribe(userType => {
      if(userType.message == 'Volunteer'){
        this.isVolunteer = true;
        this.voluntaryService.enrollVolunteer(this.voluntaryId).subscribe(res => {this.ngAfterViewInit()}); 
        let newNotification = new Notification();
            newNotification.data = new Date();
            newNotification.text = "Alguém se inscreveu no seu voluntariado!";
            newNotification.title = "Novo voluntário!";
            newNotification.user_id = this.userId;
            newNotification.state = "Nova"
            this.notificationService.postNotification(newNotification).subscribe(res => {
              if(res.success) {
                console.log("entrou");
              } else {
                console.log("não entrou");
              }
            });
      }  
    });
  }

  deroll(){
    this.isInvolved = false;
    this.userService.getUserType().subscribe(userType => {
      if(userType.message == 'Volunteer'){
        this.isVolunteer = true;
        this.voluntaryService.derollVolunteer(this.voluntaryId).subscribe(res => {this.ngAfterViewInit()});
      }
   
    });
  }

  refresh(): void {
    window.location.reload();
  }
}
