import { Component, OnInit } from '@angular/core';
import { Voluntary } from "src/app/shared/voluntary/voluntary.model";
import { VoluntaryService } from "src/app/shared/voluntary/voluntary.service";
import { UserService } from 'src/app/shared/user.service';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-voluntary-profile',
  templateUrl: './voluntary-profile.component.html',
  styleUrls: ['./voluntary-profile.component.css', '../../app.component.css', '../../../css/utilAuthForm.css']
})
export class VoluntaryProfileComponent implements OnInit {
  voluntary: Voluntary;
  areas: [String];
  date: String;

  geocoder: any;
  map: any;

  constructor(private voluntaryService: VoluntaryService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    var voluntaryId = window.location.href.split("/")[4];

    this.voluntaryService.getVoluntary(voluntaryId).subscribe(voluntary => {
      this.voluntary = voluntary;
      this.areas = voluntary.voluntary_areas;

      this.initMap(this.voluntary.address);

      this.date = moment(voluntary.date).lang("pt").format('LL');
    });
  }

  initMap(address) {
    var map = new google.maps.Map(document.getElementById("map"), {
      zoom: 15,
      center: { lat: -34.397, lng: 150.644 }
    });
    var geocoder = new google.maps.Geocoder();
  
    this.geocodeAddress(geocoder, map, address);
  }

  geocodeAddress(geocoder, resultsMap, address) {
    geocoder.geocode({ address: address }, function(results, status) {
      if (status === "OK") {
        console.log(results);
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
}
