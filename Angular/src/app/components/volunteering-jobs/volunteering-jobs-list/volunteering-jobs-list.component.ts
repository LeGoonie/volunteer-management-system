import { Voluntary } from './../../../shared/voluntary/voluntary.model';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { VoluntaryService } from './../../../shared/voluntary/voluntary.service';
import * as moment from 'moment';

export interface VolunteeringJobsListItem {
  _id: string;
  voluntaryName: string;
  description: string;
  voluntary_areas: string;
  date: string;
}

const volunteeringJobsList = [];

@Component({
  selector: 'app-volunteering-jobs-list',
  templateUrl: './volunteering-jobs-list.component.html',
  styleUrls: ['./volunteering-jobs-list.component.css'],
})
export class VolunteeringJobsListComponent implements AfterViewInit, OnInit {
  constructor(private voluntaryService: VoluntaryService) {}

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<VolunteeringJobsListItem>;
  dataSource = new MatTableDataSource(volunteeringJobsList);
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'voluntaryName',
    'description',
    'voluntary_areas',
    'date'
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    volunteeringJobsList.splice(0, volunteeringJobsList.length);

    this.voluntaryService.getAllVoluntaries().subscribe((res) => {
      const item = JSON.parse(res['_body']);
      item.forEach((element) => {
        element.date = moment(element.date).locale('pt').format('LL');
        volunteeringJobsList.push(element);
      });
    });
    setTimeout(() => {
      this.dataSource = new MatTableDataSource(volunteeringJobsList);
    this.table.dataSource = this.dataSource;
    }, 1500)
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
