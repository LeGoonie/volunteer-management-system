import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteeringJobsComponent } from './volunteering-jobs.component';

describe('VolunteeringJobsComponent', () => {
  let component: VolunteeringJobsComponent;
  let fixture: ComponentFixture<VolunteeringJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolunteeringJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteeringJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
