import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../shared/user.service';
import { Observable } from 'rxjs';

@Injectable()
export class EntityGuard implements CanActivate {
    constructor (private userService: UserService, private router: Router){
      
    }

    canActivate() : boolean | Observable<boolean> | Promise<boolean>{
      if(this.userService.getUserType().subscribe(userType => {
        if(userType.message == 'Entity'){
          return true;
        } else {
          this.router.navigate(['/']);
          return false;
        }
      })){
        return true
      } else {
        return false
      }
    }
}