import { User } from '../user.model'

export class Entity {
    _id: String;
    user: User;
    nome: String;
    description: String;
}