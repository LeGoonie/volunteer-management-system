import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { Entity } from './entity.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class EntityService{
    entity: any;
    selectedEntity: Entity;
    entities: Entity[];
    readonly baseURL = apiUrl + '/entity';

    constructor(private http: Http) { }

    postEntity(entity: Entity){
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.baseURL, entity, {headers: headers}).map(res => res.json());
    }

    getEntities(){
        return this.http.get(this.baseURL);
    }

    getEntityByName(entityName: String){
        return this.http.get(this.baseURL + '/getByName/' + entityName).map(res => res.json());
    }

    putEntity(entity: Entity){
        return this.http.put(this.baseURL + `/${entity._id}`, entity);
    }

    deleteEntity(_id: String){
        return this.http.delete(this.baseURL + `/${_id}`);
    }
    
    getLoggedEntity(){
        let headers = new Headers();
        let token = localStorage.getItem('id_token')
        headers.append('Authorization', token);
        headers.append('Content-Type','application/json');
        return this.http.get(this.baseURL + '/logged_entity/entity', { headers: headers})
        .map(res => res.json());
    }

    
}