import { User } from '../user.model';

export class Notification {
    _id: string;
    user_id: string;
    title: string;
    text: string;
    state: string;
    data: Date;
}