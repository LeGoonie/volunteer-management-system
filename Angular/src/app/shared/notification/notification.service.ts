import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { Notification } from './notification.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class NotificationService {
  authToken: any;
  notification: any;
  notifications: Notification[];
  readonly baseURL = apiUrl + '/notification';

  constructor(private http: Http) { }

  postNotification(notification: Notification){
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      return this.http.post(this.baseURL, notification, {headers: headers}).map(res => res.json());
  }

    getNotifications(){
        return this.http.get(this.baseURL);
    }

    getNotification(id: String){
        return this.http.get(this.baseURL + '/notificationId' + `/${id}`).map(res => res.json());
    }

    getNotificationsOfUser(_id : String){
        return this.http.get(this.baseURL + `/${_id}`);
    }

    deleteNotification(_id: String){
        return this.http.delete(this.baseURL + `/${_id}`);
    }

    editNotification(_id: String, new_title: String, new_text: String, new_state: String){
        return this.http.put(this.baseURL + '/' + _id, {title: new_title, text: new_text, state: new_state})
        .map(res => res.json());
    }
}
