import { User } from '../user.model';
import { Voluntary } from '../voluntary/voluntary.model';

export class Proposal {
    _id: string;
    voluntary: Voluntary;
    state: boolean;
    submissionDate: Date;
}