import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { Proposal } from './proposal.model';
import { Voluntary } from '../voluntary/voluntary.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class ProposalService{
    user: any;
    selectedProposal: Proposal;
    users: Proposal[];
    readonly baseURL = apiUrl +'/proposal';

    constructor(private http: Http) { }

    getProposals(){
      return this.http.get(this.baseURL)
        .map(res => res.json());
    }

    getProposal(id){
      return this.http.get(this.baseURL + '/' + id)
        .map(res => res.json());
    }

    getEntityProposals(_id : String){
      return this.http.get(this.baseURL + '/entityProposals' + `/${_id}`)
    }

    postProposal(voluntary: Voluntary){
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      return this.http.post(this.baseURL, voluntary, {headers: headers}).map(res => res.json());
    }

    acceptProposal(id){
      return this.http.delete(this.baseURL + '/' + id)
        .map(res => res.json());
    }

    rejectProposal(id){
      return this.http.put(this.baseURL + '/' + id, {state: "Rejeitada"})
        .map(res => res.json());
    }

    deleteProposal(id){
      console.log(this.baseURL + `/${id}`)
      return this.http.delete(this.baseURL + "/" + id).map(res => res.json());
    }

    editProposal(id, new_voluntary, new_submissionDate){
      return this.http.put(this.baseURL + '/' + id, {voluntary: new_voluntary, submissionDate: new_submissionDate})
        .map(res => res.json());
    }

    
}