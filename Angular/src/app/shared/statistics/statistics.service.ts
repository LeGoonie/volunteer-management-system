import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Voluntary } from '../voluntary/voluntary.model';
import { Volunteer } from '../volunteer/volunteer.model';
import { User } from '../user.model';
import { Proposal } from '../proposal/proposal.model';
import { Entity } from '../entity/entity.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  readonly baseURL = apiUrl + '/statistics';

  constructor(private http: Http) { }

  getTotalNumberOfVoluntaries(){
    return this.http.get(this.baseURL + '/voluntaries/total').map(res => res.json());
  }

  getAverageVolunteerPerVoluntary(){
    return this.http.get(this.baseURL + '/voluntaries/averagePerVoluntary').map(res => res.json());
  }

  getTotalNumberOfVolunteers(){
      return this.http.get(this.baseURL + '/volunteer/total').map(res => res.json());
    }

  getTotalNumberOfProposals(){
      return this.http.get(this.baseURL + '/proposals/total').map(res => res.json());
    }

  getAcceptedRejectedPending(){
    return this.http.get(this.baseURL + '/proposals/acceptedRejectedPending').map(res => res.json());
  }  

  getTotalNumberOfEntities(){
    return this.http.get(this.baseURL + '/entity/total').map(res => res.json());
  }

  getTotalNumberOfUsers(){
    return this.http.get(this.baseURL + '/users/total').map(res => res.json());
  }

  getTotalNumberOfAdmins(){
    return this.http.get(this.baseURL + '/users/admin/total').map(res => res.json());
  }

  getAreasPerVoluntary(){
    return this.http.get(this.baseURL + '/voluntaries/areas/voluntaries').map(res => res.json());
  }
}
