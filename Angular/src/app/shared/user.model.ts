export class User {
    _id: String;
    fullName: String;
    email: String;
    phone: Number;
    address: String;
    birthDate: Date;
    password:String;
    admin:String;
    photoPath:String;
    suspended: String;
}