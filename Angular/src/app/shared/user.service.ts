import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { User } from './user.model';
import { apiUrl } from '../../apiUrl.loader';

@Injectable()
export class UserService{
  authToken: any;
  user: any;
  selectedUser: User;
  users: User[];
  readonly baseURL = apiUrl;

  constructor(private http: Http) { }

  postUser(user: User) {
    user.admin = "false";
    return this.http.post(this.baseURL + '/users', user);
  }

  getUser(_id: String){
    return this.http.get(this.baseURL + '/users' + `/${_id}`);
  }

  getUserList() {
    return this.http.get(this.baseURL + '/users');
  }

  getUserIdByEmail(email: String){
    return this.http.get(this.baseURL + '/users/email' + `/${email}`);
  }

  getSuspendedByEmail(email: String){
    return this.http.get(this.baseURL + '/users/suspend' + `/${email}`);
  }

  putUserSuspended(user: User){
    return this.http.get(this.baseURL + '/users/suspended/' + user._id); 
  }
  
  putUserUnsuspended(user: User){
    return this.http.get(this.baseURL + '/users/unsuspend' + `/${user._id}`);
  }
  
  putUser(user: User) {
    return this.http.put(this.baseURL + '/users' + `/${user._id}`, user);
  }

  deleteUser(_id: string) {
    return this.http.delete(this.baseURL + '/users' + `/${_id}`);
  }

  register(user: User){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.baseURL + '/register', user, {headers: headers}).map(res => res.json());
  }

  activeAccount(token){
    return this.http.put(this.baseURL + '/activate' + `/${token}`, token).map(res => res.json());
  }

  login(user: User){
    return this.http.post(this.baseURL + '/login', user);
  }

  recoveryPassword(user: User){
    return this.http.post(this.baseURL + '/recoverypassword', user)
      .map(res => res.json());
  }

  recoveryPasswordConfirm(token, user: User){
    return this.http.post(this.baseURL + '/recoverypasswordconfirm'+ `/${token}`, user)
      .map(res => res.json());
  }

  changePassword(user: User){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.post(this.baseURL + '/changePassword', user, {headers: headers})
      .map(res => res.json());
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    console.log(JSON.stringify(user));
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  getProfile(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.get(this.baseURL + '/profile', {headers: headers})
      .map(res => res.json());
  }

  getOtherUserProfile(id){
    return this.http.get(this.baseURL + '/profile/' + id)
      .map(res => res.json());
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn(){
    return tokenNotExpired('id_token');
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  changePhoto(photoPath: String){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.post(this.baseURL + '/changePhoto', {new_photoPath: photoPath}, {headers: headers})
      .map(res => res.json());
  }

  uploadPhoto(formData){
    return this.http.post(this.baseURL + '/file', formData)
    .map(res => res.json());
  }

  editProfile(name: String, phone: Number, address: String){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.put(this.baseURL + '/editProfile', {new_name: name, new_phone: phone, new_address: address}, {headers: headers})
      .map(res => res.json());
  }

  getProposals(){
    return this.http.get(this.baseURL + '/proposal')
      .map(res => res.json());
  }

  getUserType(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.get(this.baseURL + '/users/logged_user/user_type', {headers: headers})
      .map(res => res.json());
  }

  getUserTypeById(_id: string){
    return this.http.get(this.baseURL + '/users/user_type/' + _id).map(res=>res.json());
  }

  isType(role){
    this.getUserType().subscribe(userType => {
      if(userType.message == role){
        return true;
      } else {
        return false;
      }
    },
    err => {
      console.log(err);
      return false;
    });
  }
}
