import { Volunteer } from '../volunteer/volunteer.model'
import { Entity } from '../entity/entity.model'

export class Voluntary {
    _id: string;
    voluntaryName: string;
    volunteers: Volunteer[];
    entity: Entity;
    description: string;
    address: String;
    area: string;
    goals: string;
    targetAudience:string;
    activityDescription:string;
    specificTraining:string;
    date: Date;
    entitiesInvolved: String[];
    voluntary_areas: string[];
    observations: string;
}