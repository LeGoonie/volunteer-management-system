import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Voluntary } from './voluntary.model';
import { Entity } from '../entity/entity.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class VoluntaryService{
    user: any;
    selectedVoluntary: Voluntary;
    users: Voluntary[];
    readonly baseURL = apiUrl + '/voluntary';

    constructor(private http: Http) { }

    getUsersVoluntaries(userType){
      let headers = new Headers();
      headers.append('Authorization', localStorage.getItem('id_token'));
      headers.append('Content-Type','application/json');
      return this.http.get(this.baseURL + '/logged/voluntaries/' + userType, {headers: headers})
        .map(res => res.json());
    }

    getUsersVoluntariesById(id, userType){
      return this.http.get(this.baseURL + '/voluntaries/' + id + '/' + userType)
        .map(res => res.json());
    }

    getAllVoluntaries(){
      return this.http.get(this.baseURL);
    }

    createVoluntary(voluntaryName:String, entity:Entity, description:String, address: String, area:String, goals:String, targetAudience:String, activityDescription:String, specificTraining:String,
      voluntary_areas:String[], observations:String, entities_involved: String){
        return this.http.post(this.baseURL, {voluntaryName: voluntaryName, entity: entity, description: description, address: address, area: area, goals: goals, targetAudience: targetAudience, 
          activityDescription: activityDescription, specificTraining: specificTraining, voluntary_areas: voluntary_areas, observations: observations, entities_involved})
        .map(res => res.json());
    }

    createVoluntaryAfterAccept(voluntary: Voluntary){
      return this.http.post(this.baseURL + '/createAfterAccept', {voluntary: voluntary})
      .map(res => res.json());
    }

    getVoluntary(id){
      return this.http.get(this.baseURL + '/' + id).map(res => res.json());
    }

    deleteVoluntary(id){
      return this.http.delete(this.baseURL + '/' + id).map(res => res.json());
    }

    isVolunteerInvolved(id){
      let headers = new Headers();
      headers.append('Authorization', localStorage.getItem('id_token'));
      headers.append('Content-Type','application/json');
      return this.http.get(this.baseURL + '/isUserInvolved/' + id, {headers: headers})
        .map(res => res.json());
    }

    enrollVolunteer(id){
      let headers = new Headers();
      let token = localStorage.getItem('id_token');
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      return this.http.get(this.baseURL + '/' + id + '/volunteerEnroll', {headers: headers})
      .map(res => {
        res.json();
        });
    }

    derollVolunteer(id){
      let headers = new Headers();
      let token = localStorage.getItem('id_token');
      headers.append('Authorization', token);
      headers.append('Content-Type', 'application/json');
      return this.http.get(this.baseURL + '/' + id + '/volunteerDeroll', {headers: headers})
      .map(res => {
        res.json();
        });
    }
}