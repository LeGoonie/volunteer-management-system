import { User } from '../user.model'

export class Volunteer {
    _id: string;
    user: User;
    typeOfMember: string;
    schoolService: string;
    course: string;
    observations: string;
    interestAreas:string[];
    admin:string;
    reasonsVolunteer:string[];
}