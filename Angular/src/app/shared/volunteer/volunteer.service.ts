import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { Volunteer } from './volunteer.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class VolunteerService{
    authToken: any;
    volunteer: any;
    selectedVolunteer: Volunteer;
    volunteers: Volunteer[];
    readonly baseURL = apiUrl + '/volunteer';

    constructor(private http: Http) { }

    postVolunteer(volunteer: Volunteer){
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        return this.http.post(this.baseURL, volunteer, {headers: headers}).map(res => res.json());
    }

    getVolunteers(){
        return this.http.get(this.baseURL);
    }

    getVolunteer(_id : String){
        return this.http.get(this.baseURL + `/${_id}`);
    }

    /*putVoluterr(volunteer: Volunteer){

    }*/

    deleteVolunteer(_id: String){
        return this.http.delete(this.baseURL + `/${_id}`);
    }

    getVolunteersByInterestArea(areas, voluntary_id){
        return this.http.post(this.baseURL + '/findBy/InterestArea', {areas: areas, voluntaryId: voluntary_id}).map(res => res.json());;
    }

    
}