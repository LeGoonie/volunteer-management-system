const areas = [
    {name: "Atividades Académicas (por ex. apoio às matrículas…)"},
    {name: "Ambiental (por ex. ações de sensibilização, de limpeza…)"},
    {name: "Apoio a Eventos"},
    {name: "Informática (por ex. criação de sites, de bases de dados, formação…)"},
    {name: "Comunicação (por ex. elaboração de materiais de divulgação, como cartazes…"},
    {name: "Cultural (por ex. teatro; música…)"},
    {name: "Desporto (por ex. apoio a eventos desportivos, caminhadas…"},
    {name: "Educação (por ex. estudo acompanhado, alfabetização…)"},
    {name: "Saúde (por ex. rastreios, ações de sensibilização…)"},
    {name: "Social (por ex. apoio a idosos, a crianças…)"},
    {name: "Outro [campo de escrita livre]"}
]
