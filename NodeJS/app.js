const express = require("express");
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const { mongoose } = require('./db.js');
const config = require('./config/database');

const routesapi = require('./routes/routes.js');
const routesUser = require('./routes/routesUser.js');
const routesEntity = require('./routes/routesEntity.js');
const routesProposal = require('./routes/routesProposal.js');
const routesVoluntary = require('./routes/routesVoluntary.js');
const routesVolunteer = require('./routes/routesVolunteer.js');
const routesNotification = require('./routes/routesNotification.js');
const routesStatistics = require('./routes/routesStatistics.js');


const app = express();

//Static folder
app.use(express.static(__dirname + '/public'));

//CORS Middleware
app.use(cors());
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

//Body Parser middleware
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api', routesapi);
app.use('/api/users', routesUser);
app.use('/api/entity', routesEntity);
app.use('/api/proposal', routesProposal);
app.use('/api/voluntary', routesVoluntary);
app.use('/api/volunteer', routesVolunteer);
app.use('/api/notification', routesNotification);
app.use('/api/statistics', routesStatistics);


//Index Route
app.get("/", function (req, res) {
    res.sendFile("index.html");
});

//ERROR Page
app.get('*', function(req, res){
    res.send('Erro, URL inválido.');
});

//Start Server
var server = app.listen(8081, function () {
    var host = server.address().address === "::" ? "localhost" :
    server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});

module.exports = app;