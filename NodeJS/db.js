const mongoose = require('mongoose');
const config = require('./config/database');

mongoose.connect('mongodb+srv://Goonie:' + config.secret + '@cluster0-k4mp8.mongodb.net/VolunteerSystemDB?retryWrites=true&w=majority', (err) => {
    if(!err)
        console.log('MongoDB connection succeeded');
    else   
        console.log('Error in DB connection: ' + JSON.stringify(err, undefined, 2));
});

// Make Mongoose use `findOneAndUpdate()`. Note that this option is `true`
// by default, you need to set it to false.
mongoose.set('useFindAndModify', false);

module.exports = mongoose;