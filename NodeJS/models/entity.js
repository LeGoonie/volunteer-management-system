const mongoose = require('mongoose');
var Schema = mongoose.Schema;


var EntitySchema = new Schema({
    user: {
        type: Object,
        required: true,
        default: null
    },
    nome: { 
        type: String,
        required: true
    },
    description: { type: String }
});

const Entity = module.exports = mongoose.model('Entity', EntitySchema);
