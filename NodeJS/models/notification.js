const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({
    user_id: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    data: {
        type: Date,
        required: true,
        default: Date.now()
    }
});

const Notification = module.exports = mongoose.model('Notification', NotificationSchema);