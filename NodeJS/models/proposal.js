const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
var moment = require('moment');
var Voluntary = require('../models/voluntary');
var Schema = mongoose.Schema;

var ProposalSchema = new Schema({
    voluntary: { 
        type: Object,
        required: true,
        default: null
    },
    state: { 
        type: String,
        default: "Pendente",
        required: true
    },
    submissionDate: {
        type: Date,
        default: Date.now(),
        required: true
    }
});

const Proposal = module.exports = mongoose.model('Proposal', ProposalSchema) ;