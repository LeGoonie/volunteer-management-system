const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    fullName: { 
        type: String,
        required: true
    },
    email: { 
        type: String,
        required: true
    },
    phone: { 
        type: Number
    },
    address: { 
        type: String
    },
    birthDate: { 
        type: Date
    },
    password: { 
        type: String,
        required: true
    },
    admin: { 
        type: Boolean,
        required: true,
        default: false
    },
    photoPath: {
        type: String,
        required:true
    },
    active: { 
        type: Boolean, 
        required: true, 
        default: false 
    },
    temporaryToken: { 
        type: String, 
        required: true
    },
    suspended: {
        type: Boolean,
        default: false,
        required:true
    }
});

const User = module.exports = mongoose.model('User', UserSchema) ;

module.exports.getUserById = function(id, callback){
  User.findById(id, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
}