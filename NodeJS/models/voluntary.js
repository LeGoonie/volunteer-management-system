const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VoluntarySchema = new Schema({
    voluntaryName: {
        type: String,
        required: true
    },
    volunteers: {
        type: [Object],
        default: null
    },
    entity: {
        type: Object,
        required: true,
        default: null
    },
    description: {
        type: String,
        required: true,
        default: "Voluntariado sem resumo."
    },
    address: {
        type: String
    },
    area: {
        type: String,
        required: true
    },
    goals: {
        type: String
    },
    targetAudience: {
        type: String,
        required: true
    },
    activityDescription: {
        type: String,
        required: true
    },
    specificTraining: {
        type: String,
    },
    date: {
        type: Date,
        default: new Date().toJSON().slice(0,10)
    },
    entitiesInvolved: {
        type: [String]
    },
    voluntary_areas: {
        type: [String],
        required:true
    },
    observations: {
        type: String,
    }
});

const Voluntary = module.exports = mongoose.model('Voluntary', VoluntarySchema);