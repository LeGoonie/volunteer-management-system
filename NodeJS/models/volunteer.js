const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VolunteerSchema = new Schema ({
    user: {
        type: Object,
        required: true,
        default: null
    },
    typeOfMember: { 
        type: String,
        required: true 
    },
    schoolService: { 
        type: String,
        default: null
    },
    course: { 
        type: String,
        default: null
    },
    observations: { 
        type: String,
        default: null
    },
    interestAreas: {
        type: [String],
        default: null
    },
    reasonsVolunteer: { 
        type: [String],
        default: null
    }
});

const Volunteer = module.exports = mongoose.model('Volunteer', VolunteerSchema);