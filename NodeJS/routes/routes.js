const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Entity = require('../models/entity');
const Proposal = require('../models/proposal');
const ObjectId = require('mongoose').Types.ObjectId;
const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('../config/database');
const sendgridConfig = require('../config/sendgrid');
const sgMail = require('@sendgrid/mail');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const multer = require('multer');

sgMail.setApiKey(sendgridConfig.api_key);

//let upload = multer({ dest: 'Angular/src/app/assets/images/' });

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, '../Angular/src/assets/images/')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
});
  
const upload = multer({ storage: storage });


// => localhost:8081/api/users/register
router.get('/register', (req,res) => {
    res.send("REGISTER");
});

router.post('/register', (req, res) => {
    var newUser = new User({
        fullName: req.body.fullName,
        email: req.body.email,
        phone: req.body.phone,
        address: req.body.address,
        birthDate: req.body.birthDate,
        password: req.body.password,
        admin: req.body.admin,
        photoPath: "../../../assets/images/default_profile.png",
        temporaryToken: jwt.sign({ username: req.body.fullName, email: req.body.email }, config.secret, {expiresIn: '24h'})
    });

    if(req.body.password.toString() != req.body.passConfirmed.toString()){
        return false;
    }

    User.findOne({ email: newUser.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if(err) throw err;
                    newUser.password = hash;
                    newUser.save((err, doc) => {
                        if(!err) {
                            const msg = {
                                to: newUser.email,
                                from: 'volunteer.platform.ips@gmail.com',
                                templateId: 'd-5c9b511df6a44345918a7434c755067c',
                                dynamic_template_data: {
                                    subject: 'Link para ativação do sistema de gestão de voluntariados',
                                    fullName: newUser.fullName,
                                    temporaryToken: newUser.temporaryToken
                                }
                            };
                            sgMail.send(msg).then(() => {
                                console.log('Message sent')
                            }).catch((err) => {
                                console.log(err.response.body)
                            })
                            res.json({ sucess: true, message: 'O link de verificação foi enviado para o seu email!'});
                        }
                        else { console.log('Password has been changed ' + JSON.stringify(err, undefined, 2)); }
                    });
                });
            });
        } else {
            res.json({ sucess: false, message: 'Este email ja existe no sistema!' });
        }
    });
});

router.put('/activate/:token', (req, res) => {
    User.findOne({ temporaryToken: req.params.token }, (err, user) => {
        if(err) res.json({ success: false, message: 'Algo correu mal!'});
        var token = req.params.token;

        jwt.verify(token, config.secret, (err, decoded) => {
            if(err) {
                res.json({ success: false, message: 'O link de ativação expirou.'});
            } else if (!user){
                res.json({ success: false, message: 'O link de ativação não existe.'});
            } else {
                user.temporaryToken = false;
                user.active = true;
                user.save((err) => {
                    if(err) {
                        console.log(err);
                    } else {
                        var msg = {
                            from: 'volunteer.platform.ips@gmail.com',
                            to: user.email,
                            subject: 'Conta do Sistema de Voluntariado Activada',
                            text: 'Olá ' + user.fullName + ', a tua conta no sistema de voluntariado do Instituto Politécnico de Setúbal foi corretamente ativada!',
                            html: 'Olá <strong>' + user.fullName + '</strong>, <br><br>A tua conta no sistema de voluntariado do Instituto Politécnico de Setúbal foi corretamente ativada!'
                        };
                        sgMail.send(msg).then(() => {
                            console.log('Message sent')
                        }).catch((err) => {
                            console.log(err.response.body)
                        })
                        res.json({ success: true, message: 'Conta ativada!.'});
                    }
                });
            }
        });
    })
});

router.post('/login', (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;

    if(email == "" || password == ""){
        res.json({ success: false, message: 'Preencha todos os campos' });
    } else {
        User.findOne({ email: req.body.email }).select().exec((err, user) => {
            if(err) { throw err; }
            if(!user) {
                res.json({ success: false, message: 'Email não existe' });
            } else if (!user.active){
                res.json({ success: false, message: 'Precisa de confirmar primeiro o seu email!' });
            } else {
                bcrypt.compare(req.body.password, user.password, (err, result)=> {
                    if (result){
                        const token = jwt.sign(user.toJSON(), config.secret, {
                            expiresIn: 604800 // 1 week
                            });
                            res.json({
                            success: true,
                            token: 'Bearer ' + token,
                            user: {
                                id: user._id,
                                name: user.fullName,
                                email: user.email,
                                phone: user.phone,
                                address: user.address,
                                birthDate: user.birthDate,
                                password: user.password
                            }
                            });
                    } else {
                        res.json({ sucess: false, message: 'Password errada' });
                    }
                });
            }
        });
    }
});


// => localhost:8081/api/users/change_password/:email
// Muda a password de um utilizador
router.post('/changePassword', passport.authenticate('jwt', { session: false }), (req, res) => {        
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            if(req.body.new_password != req.body.new_password_v2){
                res.json({ sucess: false, message: 'Passwords não são iguais' });
            } else if(req.body.new_password == undefined || req.body.new_password_v2 == undefined || req.body.old_password == undefined){
                res.json({ sucess: false, message: 'Preencha todos os campos' });
            } else {
                bcrypt.compare(req.body.old_password, user.password, (err, result)=> {
                    if (result){
                        bcrypt.genSalt(10, (err, salt) => {
                            bcrypt.hash(req.body.new_password, salt, (err, hash) => {
                                if(err) throw err;
                                user.password = hash;

                                user.save((err, doc) => {
                                    if(!err) { res.json({ sucess: true, message: 'Password alterada' }); }
                                    else { console.log('Password has been changed ' + JSON.stringify(err, undefined, 2)); }
                                });
                            });
                        });
                    } else {
                        res.json({ sucess: false, message: 'Password antiga não está correta' });
                    }
                });
            }
        }
    });
});

// => localhost:8081/api/recoverypassword
router.post('/recoverypassword', (req, res) => {
    User.findOne({ email: req.body.email }).select().exec((err, user) => {
        if(err) { throw err; }
        if(!user) {
            res.json({ sucess: false, message: 'O email indicado não está registado no sistema' });
        } else {
            user.temporaryToken = jwt.sign({ username: req.body.fullName, email: req.body.email }, config.secret, {expiresIn: '24h'});

            user.save((err, doc) => {
                if(!err) { 
                    const msg = {
                        to: user.email,
                        from: 'volunteer.platform.ips@gmail.com',
                        subject: 'Recuperação de senha do Sistema de Voluntariado do IPS',
                        text: 'Olá ' + user.fullName + ', para redefinir a sua senha de acesso por favor clique no link abaixo: http://localhost:4200/recupPassConfirm/' + user.temporaryToken,
                        html: 'Olá <strong>' + user.fullName + '</strong>, <br><br>Para redefinir a sua senha de acesso por favor clique no link abaixo, <br><br><a href="http://35.238.196.140/recupPassConfirm/' + user.temporaryToken + '">http://35.238.196.140/recupPassConfirm/</a>'
                    };
                    sgMail.send(msg).then(() => {
                        res.json({ sucess: true, message: 'O link para redefinição de senha foi enviado para o seu email!' });
                    }).catch((err) => {
                        res.json({ sucess: false, message: 'Algo não correu conforme o esperado' });
                    })
                }
                else { res.json({ sucess: false, message: 'Algo não correu conforme o esperado' }); }
            });
        }
    });
});

// => localhost:8081/api/recoverypasswordconfirm/:token
router.post('/recoverypasswordconfirm/:token', (req, res) => {
    var token = req.params.token;
    User.findOne({ temporaryToken: token }).select().exec((err, user) => {
        if(err) { throw err; }

        jwt.verify(token, config.secret, (err, decoded) => {
            if(err) {
                res.json({ success: false, message: 'O link de redefinição de senha expirou.'});
            } else if (!user){
                res.json({ success: false, message: 'O link de redefinição de senha expirou.'});
            } else {
                if(req.body.new_password != req.body.new_password_v2){
                    res.json({ sucess: false, message: 'Passwords não são iguais' });
                } else if(req.body.new_password == undefined || req.body.new_password_v2 == undefined){
                    res.json({ sucess: false, message: 'Preencha todos os campos' });
                } else {
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(req.body.new_password, salt, (err, hash) => {
                            if(err) throw err;
                            user.temporaryToken = false;
                            user.password = hash;

                            user.save((err, doc) => {
                                if(!err) { 
                                    var msg = {
                                        from: 'volunteer.platform.ips@gmail.com',
                                        to: user.email,
                                        subject: 'Senha do Sistema de Voluntariado redefinida',
                                        text: 'Olá ' + user.fullName + ', a tua senha do sistema de voluntariado do Instituto Politécnico de Setúbal foi redefinida com sucesso!',
                                        html: 'Olá <strong>' + user.fullName + '</strong>, <br><br>A tua senha do sistema de voluntariado do Instituto Politécnico de Setúbal foi redefinida com sucesso!'
                                    };
                                    sgMail.send(msg).then(() => {
                                        console.log('Message sent')
                                    }).catch((err) => {
                                        console.log(err.response.body)
                                    })
                                    res.json({ success: true, message: 'Senha redefinida com sucesso!'});  
                                }
                                else { 
                                    res.json({ success: false, message: 'Algo correu mal ao alterar a senha de acesso! Por favor tente novamente mais tarde.'});
                                }
                            });
                        });
                    });
                }
            }
        });
    });
});

//Profile
router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({
        user: {
            _id: req.user._id,
            fullName: req.user.fullName,
            email: req.user.email,
            phone: req.user.phone,
            address: req.user.address,
            birthDate: req.user.birthDate,
            photoPath: req.user.photoPath
        }
    });
});

router.get('/profile/:id', (req, res, next) => {
    User.findOne({ _id: req.params.id }).select().exec((err, user) => {
        if(err) { throw err; }
        if(!user) {
            res.json({ success: false, message: 'O ID indicado não está registado no sistema' });
        } else {
            res.json({ success: true,
                user: {
                    _id: user._id,
                    fullName: user.fullName,
                    email: user.email,
                    phone: user.phone,
                    address: user.address,
                    birthDate: user.birthDate,
                    photoPath: user.photoPath
                }
            });
        }
    });
});

router.post('/file', upload.single('file'), (req, res, next) => {
    const file = req.file;
    var random_filename = generate_token(32) + '.' + file.originalname.split('.')[1];
    fs.rename(file.destination + file.originalname, file.destination + random_filename, function(err) {
        if ( err ) console.log('ERROR: ' + err);
    });

    if (!file) {
      const error = new Error('No File')
      error.httpStatusCode = 400
      return next(error)
    }

    res.json({ sucess: true, message: 'Foto foi uploaded!', originalName: random_filename });
})

// => localhost:8081/api/changePhoto
// Muda o caminho da foto de um utilizador
router.post('/changePhoto', passport.authenticate('jwt', { session: false }), (req, res) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            const split_string = user.photoPath.split("/");
            
            if(split_string[5] != "default_profile.png"){
                fs.unlink('../Angular/src/assets/images/' + split_string[5], (err) => {
                  if (err) throw err;
                  console.log('successfully deleted');
                });
            }
            
            user.photoPath = req.body.new_photoPath;
    
            user.save((err, doc) => {
                if(!err) { res.json({ sucess: true, message: 'Foto alterada' }); }
            });
        }

        
    });
});

// => localhost:8081/api/editProfile
// Altera variáveis do perfil do utilizador
router.put('/editProfile', passport.authenticate('jwt', { session: false }), (req, res) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            if(req.body.new_name != undefined){ user.fullName = req.body.new_name; }
            if(req.body.new_phone != undefined){ user.phone = req.body.new_phone; }
            if(req.body.new_address != undefined){ user.address = req.body.new_address; }

            user.save((err, doc) => {
                if(!err) { res.json({ sucess: true, message: 'Perfil editado' }); }
            });
        }
    });
});

function generate_token(length){
    //edit the token allowed characters
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];  
    for (var i=0; i<length; i++) {
        var j = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}

module.exports = router;
