const express = require('express');
const router = express.Router();
const Entity = require('../models/entity');
const User = require('../models/user');
const passport = require('passport');

// => localhost:8081/api/entity
// Cria uma entidade
router.post('/', (req, res) => {
    var newEntity = new Entity({
        nome: req.body.nome,
        description: req.body.description,
    });

    User.findOne({ _id: req.body.user_id }).select().exec((err, user) => {
        if(err){
            throw err;
        }
        if(user) {
            newEntity.user = user;

            Entity.findOne({ nome: newEntity.nome }).select().exec((err, entity) => {
                if(err){
                    throw err;
                }
                if(!entity) {
                    newEntity.save((err, doc) => {
                        if(!err){
                            res.json({ sucess: true, message: 'Entidade criada'});
                        } else {
                            res.json({message: "Erro"});
                        }
                    });
                } else {
                    res.json({success: false, message: 'Esta entidade ja existe no sistema!'})
                }
            });
        } else {
            res.json({success: false, message: 'Este user não existe!'})
        }
    });   
});


// => localhost:8081/api/entities
// Retornas todas as entidades
router.get('/', (req, res) => {
    Entity.find((err,docs) => {
        if(!err) { res.send(docs); }
        else { console.log('Error in Retriving Entities :' + JSON.stringify(err, undefined, 2))};
    });
});


// => localhost:8081/api/entity/_id
// Retorna apenas 1 entidade
router.get('/:id', (req,res)=>{
    Entity.findById(req.params.id, (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in Retriving Entity: ' + JSON.stringify(err, undefined, 2)); }
    });
});


// => localhost:8081/api/entity/_id
// Modifica uma entidade
router.put('/:id', (req, res) => {
    var us = {};
    us.user = req.body.user,
    us.name = req.body.name,
    us.description = req.body.description,
    
    Entity.findByIdAndUpdate(req.params.id, us , (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
    }); 
});


// => localhost:8081/api/entity/_id
// Apaga uma entidade
router.delete('/:id', (req, res) => {
    Entity.findByIdAndDelete(req.params.id, (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in Entity Delete: ' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/logged_entity/entity', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        let msg;
        if(err) { throw err; }
        if(!user) {
            res.json({ success: false, message: 'Utilizador não existe' });
        } else {
            Entity.find((err, entities) => {
                if(err) { throw err; }
                entities.forEach(entity => {
                    if(entity.user._id.toString() == user._id.toString()){
                        msg = entity
                    }
                });
                if(!msg){
                    res.json({ success: false, message: "O utilizador loggado não é uma entidade" }); 
                } else {
                    res.json({ success: true, message: msg });
                }
            });
        }
    });
});

router.get('/getByName/:name', (req, res) => {
    Entity.findOne({ nome: req.params.name }).select().exec((err, entity) => {
        if(err){
            throw err;
        }
        if(entity) {
            res.json({success: true, value: entity});
        } else {
            res.json({success: false, message: 'Não existe uma entidade com esse nome!'})
        }
    });
});

module.exports = router;