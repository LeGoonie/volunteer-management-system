const express = require('express');
const router = express.Router();
const Notification = require('../models/notification');
const User = require('../models/user');

// => localhost:8081/api/notification/_id
// Retornas todas as notificacoes para um user
router.get('/:id', (req, res) => {
    var user_id = req.params.id;

    var user_notifications = [];

    User.findOne( { _id: user_id}).select().exec((err, user) => {
        if(err){
            res.json( {sucess: false, message: 'User _id invalido'} );
        } else {
            Notification.find((err,notifications) => {
                if(!err) { 
                    notifications.forEach(notification => {
                        if(notification.user_id.toString() == user_id.toString()){
                            user_notifications.push(notification);
                        }
                    });
                    
                    if(user_notifications.length != 0){
                        res.json(user_notifications);
                    }else {
                        res.json('Deste momento não tem notificações');
                    }
                    
                }
                else {
                    res.json({ sucess: false, message: err});
                }
            });
        }     
    });
});

router.get('/notificationId/:id', (req, res) => {
    Notification.findOne( { _id: req.params.id}).select().exec((err, notification) => {
        if(err){
            res.json( {sucess: false, message: 'Id da notificação inválido'} );
        } else {
            res.json({sucess: true, message: notification });
        }     
    });
});

// => localhost:8081/api/notification/
// Cria uma notificação com user_id mandado por body
router.post('/', (req, res) => {
    var newNotification = new Notification({
        title: req.body.title,
        text: req.body.text,
        state: req.body.state
    });

    User.findOne( { _id: req.body.user_id}).select().exec((err, user) => {
        if(err){
            res.json( {sucess: false, message: err} );
        }
        if(user){
            newNotification.user_id = req.body.user_id;
            newNotification.save((err, notification) => {
                if(!err){
                    res.json(notification);
                } else {
                    res.json( {sucess: false, message: err})
                }
            });
        } else {
            res.json( {sucess: false, message: "Utilizador não encontrado"});
        }
    });
});

// => localhost:8081/api/notification/_id
// Apaga uma notificação dando o seu id
router.delete('/:id', (req, res) => {
    Notification.findByIdAndDelete(req.params.id, (err, notification) => {
        if(!err) {res.json(notification)}
        else {
            res.json( {sucess: false, message: 'Id do utilizador não encontrado'});
        }
    });
});

// => localhost:8081/api/notification
// Retornas todas as notificacoes para um user
router.get('/', (req,res) => {
    Notification.find((err, notificacoes) => {
        res.json(notificacoes);
    });
});

// => localhost:8081/api/notification/id
// Edita uma notificação
router.put('/:id', (req, res) => {
    Notification.findOne({ _id: req.params.id }).select().exec((err, notification) => {
        if(err){
            throw err;
        }
        if(notification) {
            if(req.body.title != undefined){
                notification.title = req.body.title;
            }

            if(req.body.text != undefined){
                notification.text = req.body.text;
            }

            if(req.body.state != undefined){
                notification.state = req.body.state;
            }

            notification.data = Date.now();

            notification.save((err, doc) => {
                if(!err) { res.json({ success: true, message: 'Notificação editada' }); }
            });
            
        } else {
            res.json({success: false, message: 'Não existe uma notificação para editar com esse id'})
        }
    });
});

module.exports = router;


