const express = require('express');
const router = express.Router();
const Voluntary = require('../models/voluntary');
const User = require('../models/user');
const Proposal = require('../models/proposal');
const Entity = require('../models/entity');

// => localhost:8081/api/proposal
// Devolve todas as propostas
router.get('/', (req, res) => {
    Proposal.find((err,docs) => {
        if(!err) { res.send(docs); }
        else { console.log('Error in Retriving Proposals :' + JSON.stringify(err, undefined, 2))};
    });
});

// => Localhost:8081/api/proposal/:entityId
//Retorna os voluntariados de uma certa entidade
router.get('/entityProposals/:entityId', (req,res)=>{
    const props = [];
    Proposal.find((err, proposals) => {
        if(err){ 
            throw err; 
        } else { 
            proposals.forEach(proposal => {
                if(proposal.voluntary.entity._id == req.params.entityId){
                    props.push(proposal)
                }
            });
        };
        res.send(props);
    })
});

// => localhost:8081/api/proposal/:id
// Devolve uma proposta específica a partir do ID do voluntariado
router.get('/:id', (req, res) => {
    Proposal.findOne({ _id: req.params.id }).select().exec((err, proposal) => {
        if(err){
            throw err;
        }
        if(proposal) {
            res.send(proposal);
        } else {
            res.json({success: false, message: 'Não existe uma proposta com este voluntariado'})
        }
    });
});

// => localhost:8081/api/proposal
// Cria uma proposta
router.post('/', (req, res) => {
    var entitiesInvolved = []
    console.log(req.body.entitiesInvolved);
    req.body.entitiesInvolved.forEach(element => {
        Entity.findOne({nome: element}).select().exec((err, entity, i) => {
            if(!err) 
                if(entity)
                    entitiesInvolved.push(entity)
                else 
                    entitiesInvolved.push({name: element})
        });
    });
    let voluntary = new Voluntary({
        voluntaryName: req.body.voluntaryName,
        entity: req.body.entity,
        description: req.body.description,
        address: req.body.address,
        area: req.body.area,
        goals: req.body.goals,
        targetAudience: req.body.targetAudience,
        activityDescription: req.body.activityDescription,
        specificTraining: req.body.specificTraining,
        date: req.body.date,
        entitiesInvolved: this.entitiesInvolved,
        voluntary_areas: req.body.voluntary_areas,
        observations: req.body.observations
    });

    voluntary.entitiesInvolved = req.body.entitiesInvolved

    let proposal = new Proposal();
    proposal.voluntary = voluntary
    proposal.submissionDate = "Pendente"
    proposal.submissionDate = new Date()

    proposal.save((err, doc) => {
        if(!err) {
            res.json({ success: true, message: 'Proposta criada' }); 
        } else {
            res.json({ success: false, message: err });
        }
    });
});

// => localhost:8081/api/proposal/id
// Edita uma proposta
router.put('/:id', (req, res) => {
    Proposal.findOne({ _id: req.params.id }).select().exec((err, proposal) => {
        if(err){
            throw err;
        }
        if(proposal) {
            if(req.body.voluntary != undefined){
                proposal.voluntary = req.body.voluntary;

                if(req.body.voluntary.voluntaryName != undefined){
                    proposal.name = req.body.voluntary.voluntaryName;
                }
            }

            if(proposal.state == "Rejeitada"){
                proposal.state = "Pendente";
            }else if(req.body.state != undefined){
                proposal.state = req.body.state;
            }

            proposal.submissionDate = Date.now();

            proposal.save((err, doc) => {
                if(!err) { res.json({ success: true, message: 'Proposta editada' }); }
            });
            
        } else {
            res.json({success: false, message: 'Não existe uma proposta para editar com esse voluntariado'})
        }
    });
});

// => localhost:8081/api/proposal/id
// Apaga uma proposta
router.delete('/:id', (req, res) => {
    console.log("id: " + req.params.id)
    Proposal.findOneAndRemove({ _id: req.params.id }).select().exec((err, proposal) => {
        if(err){
            throw err;
        }
        if(proposal) {
            console.log('Proposta apagada!')
            res.json({success: true, message: 'Proposta apagada!', value: proposal.voluntary})
        } else {
            console.log('Não existe uma proposta para editar com esse voluntariado!')
            res.json({success: false, message: 'Não existe uma proposta para editar com esse voluntariado!'})
        }
    });
});

module.exports = router;