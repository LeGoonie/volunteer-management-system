const express = require('express');
const router = express.Router();
const Entity = require('../models/entity');
const User = require('../models/user');
const Notification = require('../models/notification');
const Proposal = require('../models/proposal');
const Voluntary = require('../models/voluntary');
const Volunteer = require('../models/volunteer');

// => localhost:8081/api/statistics/entity/total
// Numero total de entidades 
router.get('/entity/total', (req, res) => {
    var counter = 0;
    Entity.find((err, entities) => {
        if (err) res.json(err);
        else {
            entities.forEach(entity => {
                counter++;
            });
            res.json(counter);
        }
    });   
});

// => localhost:8081/api/statistics/volunteer/total
//Numero total de volunteers
router.get('/volunteer/total', (req, res) => {
    var counter = 0;
    Volunteer.find((err, volunteers) => {
        if (err) res.json(err);
        else {
            volunteers.forEach(volunteer => {
                counter++;
            });
            res.json(counter);
        }
    });   
});

// => localhost:8081/api/statistics/proposals/total
//Numero total de proposals
router.get('/proposals/total', (req, res) => {
    var counter = 0;
    Proposal.find((err, proposals) => {
        if (err) res.json(err);
        else {
            proposals.forEach(proposal => {
                counter++;
            });
            res.json(counter);
        }
    });   
});

// => localhost:8081/api/statistics/proposal/acceptedRejectedPending
// Retorna a percentagem de voluntariados aceites, rejeitados e a aguardar aprovação
router.get('/proposals/acceptedRejectedPending', (req, res) => {
    var accepted = 0;
    var rejected = 0;
    var pending = 0;
    var total = 0;

    Proposal.find((err, proposals) => {
        if (err) res.json(err);
        else {
            proposals.forEach(proposal => {
                if (proposal.state == 'Pendente') {
                    pending++;
                } else {
                    rejected++;
                }               
            });
            Voluntary.find((err, voluntaries) => {
                if (err) res.json(err);
                else {
                    voluntaries.forEach(voluntary => {
                        accepted++;
                    });
                    res.json(accepted+','+rejected+','+pending);
                }
            });
        }
    });

});

// => localhost:8081/api/statistics/users/total
//Numero total de users
router.get('/users/total', (req, res) => {
    var counter = 0;
    User.find((err, users) => {
        if (err) res.json(err);
        else {
            users.forEach(user => {
                counter++;
            });
            res.json(counter);
        }
    });   
});

// => localhost:8081/api/statistics/users/admin/total
//Numero total de admins
router.get('/users/admin/total', (req, res) => {
    var counter = 0;
    User.find((err, users) => {
        if (err) res.json(err);
        else {
            users.forEach(user => {
                console.log(user.admin);
                console.log(typeof(user.admin));
                if(user.admin == true) counter++;
            });
            res.json(counter);
        }
    });   
});

// => localhost:8081/api/statistics/voluntaries/total
// Retorna o numero de voluntariados
router.get('/voluntaries/total', (req, res) => {
    var counter = 0;
    Voluntary.find((err, voluntaries) => {
        if (err) res.json(err);
        else {
            voluntaries.forEach(voluntary => {
                counter++;
            });
            res.json(counter);
        }
    });   
});

// => localhost:8081/api/statistics/voluntaries/averagePerVoluntary
// Retorna o numero medio de voluntarios por voluntariado
router.get('/voluntaries/averagePerVoluntary', (req, res) => {
    var numberOfUsers = 0;
    var numberOfVoluntaries = 0;
    Voluntary.find((err,voluntaries) => {
        if (err) res.json(err);
        else {
            voluntaries.forEach(voluntary => {
                numberOfUsers += voluntary.volunteers.length;
                numberOfVoluntaries++;
            });
            res.json(numberOfUsers/numberOfVoluntaries);
        }
    })
})

// => localhost:8081/api/statistics/voluntaries/averagePerVoluntary
// Retorna o numero medio de voluntariados por voluntario
router.get('/volunteer/averageVoluntary', (req, res) => {
    var numberOfVolunteers = 0;
    var numberOfVoluntaries = 0;

    Volunteer.find((err,volunteers) => {
        if (err) res.json(err);
        else {
            volunteers.forEach(volunteer =>{
                numberOfVolunteers++;
            })
            Voluntary.find((err,voluntaries) => {
                if (err) res.json(err);
                else {
                    voluntaries.forEach(voluntary => {
                        numberOfVoluntaries++;
                    });
                    res.json(numberOfVoluntaries/numberOfVolunteers);
                }
            })
        }
    })
    
    
})

function mapToObj(inputMap) {
    let obj = {};

    inputMap.forEach(function(value, key){
        obj[key] = value
    });

    return obj;
}


router.get('/voluntaries/areas/voluntaries' , (req, res) => {
    let map = new Map();
    Voluntary.find((err,voluntaries) => {
        if(err) res.json(err);
        else {
            voluntaries.forEach(voluntary => {
                voluntary.voluntary_areas.forEach(area => {
                    if(area!= null){
                        if(!map.has(area.replace(/ *\([^)]*\) */g, ""))){
                            map.set(area.replace(/ *\([^)]*\) */g, ""), "1");
                        } else {
                            map.set(area.replace(/ *\([^)]*\) */g, ""), ""+(parseInt(map.get(area.replace(/ *\([^)]*\) */g, ""))) + 1) +"");
                        }
                    }
                });
            });
            var strMap = mapToObj(map);
            res.json(strMap);
        }
    })
})

module.exports = router;