const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Voluntary = require('../models/voluntary');
const Volunteer = require('../models/volunteer');
const passport = require('passport');

// => localhost:8081/api/users
// Retorna todos os users
router.get('/', (req,res) => {
    User.find((err,docs) => {
        if(!err) { res.send(docs); }
        else { res.json({success: false, message: "Erro em ver os utilizadores"});};
    });
});

// => localhost:8081/api/users/_id
// Retorna apenas 1 user
router.get('/:id', (req,res)=>{
    User.findById(req.params.id, (err, doc) => {
        if(!err) {res.send(doc); }
        else { res.json({success: false, message: "Este user id não existe"}) }
    });    
});

// => localhost:8081/api/users/suspend/:email
// Retorna se o utilizador esta suspenso ou pelo email
router.get('/suspend/:email', (req, res)=>{
    User.findOne({email: req.params.email}).select().exec((err, user) => {
        if(err) { throw err; }
        if(user) {
            res.json(user.suspended);
        } else {
            res.json({ sucess: false, message: 'Utilizador ou email não existem!' });
        }
    });
});

// => localhost:8081/api/users/suspended/_id
// Modifica a para a conta ficar suspensa
router.get('/suspended/:id', (req, res) => {
    var us = {};
    us.suspended = true,
    User.findByIdAndUpdate(req.params.id, us , (err, doc) => {
        if(!err) {res.send(doc.suspended); }
        else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/unsuspend/:id', (req, res) => {
    var us = {};
    us.suspended= false,
    User.findByIdAndUpdate(req.params.id, us , (err, doc) => {
        if(!err) {res.send(doc.suspended); }
        else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
    });
});

// => localhost:8081/api/users/email/email
// Retorna um user _id pelo email
router.get('/email/:email', (req,res)=>{
    var str = {email: req.params.email};
    User.findOne(str).select().exec((err, user) => {
        if(err) { throw err; }
        if(user) {
            res.json(user._id);
        } else {
            res.json({ sucess: false, message: 'Utilizador ou email não existem!' });
        }
    });
});

// => localhost:8081/api/users/_id
// Modifica um user
router.put('/:id', (req, res) => {
    if(!ObjectId.isValid(req.params.id))
        return res.status(400).send('No record given with given id : ${req.params.id}');
    var us = {};
    us.fullName = req.body.fullName,
    us.email = req.body.email,
    us.phone= req.body.phone,
    us.address= req.body.address,
    us.birthDate= req.body.birthDate,
    us.admin= req.body.admin,
    us.suspended= req.body.suspended,
    console.log(us);
    User.findByIdAndUpdate(req.params.id, us , (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
    });
});


// => localhost:8081/api/users/_id
// Apaga um user
router.delete('/:id', (req, res) => {
    User.findByIdAndDelete(req.params.id, (err, doc) => {
        if(!err) {
            Volunteer.find((err, volunteers) => {
                volunteers.forEach(volunteer =>{
                    if(req.params.id == volunteer.user._id){
                        Volunteer.findByIdAndDelete(volunteer._id, (err, doc) => {
                            //res.json({ success: true, message: "Volunteer apagado" });
                        });
                    }
                });
            });
            res.send(doc); 
        }
        else { console.log('Error in User Delete: ' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/logged_user/user_type', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }
        if(!user) {
            res.json({ success: false, message: 'Id de utilizador não existe' });
        } else {
            if(user.admin){
                res.json({ success: true, message: "Admin" });
            } else {
                let role;
                let Entity = require('../models/entity');
                Entity.find((err, entities) => {
                    if(err) { res.json({ success: false, message: err }); }
                    entities.forEach(entity => {
                        if(entity.user._id.toString() == user._id.toString()){
                            role = "Entity";
                        }
                    });
                    if(!role){
                        role = "Volunteer"
                    }
                    res.json({ success: true, message: role });
                });
            }
        }
    });
});

router.get('/user_type/:id', (req, res, next) => {
    User.findOne({ _id: req.params.id }).select().exec((err, user) => {
        if(err) { throw err; }
        if(!user) {
            res.json({ success: false, message: 'Id de utilizador não existe' });
        } else {
            if(user.admin){
                res.json({ success: true, message: "Admin" });
            } else {
                let role;
                let Entity = require('../models/entity');
                Entity.find((err, entities) => {
                    if(err) { res.json({ success: false, message: err }); }
                    entities.forEach(entity => {
                        if(entity.user._id.toString() == user._id.toString()){
                            role = "Entity";
                        }
                    });
                    if(!role){
                        role = "Volunteer"
                    }
                    res.json({ success: true, message: role });
                });
            }
        }
    });
});


module.exports = router;