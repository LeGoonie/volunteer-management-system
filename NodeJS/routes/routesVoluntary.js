const express = require('express');
const router = express.Router();
const passport = require('passport');
const Voluntary = require('../models/voluntary');
const Proposal = require('../models/proposal');
const Volunteer = require('../models/volunteer');
const Entity = require('../models/entity');
const User = require('../models/user');
const request = require('request');
const { response } = require('express');

router.post('/', (req, res) => {
    var newVoluntary = new Voluntary({
        voluntaryName: req.body.voluntaryName,
        entity: req.body.entity,
        description: req.body.description,
        address: req.body.address,
        area: req.body.area,
        voluntary_areas: req.body.voluntary_areas,
        targetAudience: req.body.targetAudience,
        activityDescription: req.body.activityDescription
    });

    if(req.body.goals != undefined){
        newVoluntary.goals = req.body.goals
    }

    if(req.body.observations != undefined){
        newVoluntary.observations = req.body.observations
    }

    if(req.body.specificTraining != undefined){
        newVoluntary.specificTraining = req.body.specificTraining
    }

    if(req.body.entities_involved != undefined){
        var entities_names = [];
        req.body.entities_involved.split('; ').forEach(eName => {
            eName.split(';').forEach(entityName => {
                entities_names.push(entityName)
            });
        });
    
        newVoluntary.entitiesInvolved = entities_names;
    } else {
        newVoluntary.entitiesInvolved = "";
    }

    newVoluntary.save((err, doc) => {   
        if(!err){
            res.json({ success: true, message: 'Voluntário criado!', value: newVoluntary});
        } else {
            res.json({ success: false, message: 'Uma das informações não foi inserida corretamente'});
        }
    });
});

// => localhost:8081/api/voluntary
// Retorna todos os voluntariados
router.get('/', (req, res) => {
    Voluntary.find((err,docs) => {
        if(!err) { res.send(docs); }
        else { console.log('Error in Retriving Voluntary :' + JSON.stringify(err, undefined, 2))};
    });
});


// => localhost:8081/api/voluntary/:id
// Retorna apenas 1 voluntariado
router.get('/:id', (req,res)=>{
    Voluntary.findOne({ _id: req.params.id }).select().exec((err, voluntary) => {
        if(err){
            throw err;
        }
        if(voluntary) {
            res.send(voluntary);
        } else {
            res.json({success: false, message: 'Não existe este voluntariado'});
        }
    });
        
});




// => localhost:8081/api/voluntary/_id
// Modifica um voluntariado
router.put('/:id', (req, res) => {
    var us = {};
    us.voluntaryName = req.body.voluntaryName;
    us.volunteersIds = req.body.volunteersIds;
    us.accepted = req.body.accepted;
    us.description = req.body.description;
    us.area = req.body.area;
    us.goals = req.body.goals;
    us.targetAudience = req.body.targetAudience;
    us.activityDescription = req.body.activityDescription;
    us.specificTraining = req.body.specificTraining;
    us.entitiesInvolved = req.body.entitiesInvolved;
    us.voluntary_areas = req.body.voluntary_areas;
    us.observations = req.body.observations;
    
    Voluntary.findByIdAndUpdate(req.params.id, us , (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
    }); 
});


// localhost:8081/api/voluntary/:id/volunteerEnroll
// Vai buscar o user que esta logado e pimba mete la dentro do voluntariado
router.get('/:id/volunteerEnroll', passport.authenticate('jwt', { session: false }), (req, res) => {     
    var user_id = req.user._id;
    
    var us = {}

    var volunteers_involved = [];

    //Voluntario logado
    var volunteerStatic = {};

    Volunteer.find((err,volunteerss) => {
        if(!err) { 
            volunteerss.forEach(volunteer1 => {
                if(volunteer1.user._id.toString() == user_id.toString()){
                    volunteerStatic = volunteer1;

                    volunteers_involved.push(volunteerStatic);

                    Voluntary.findById(req.params.id, (err, voluntary) => {
                        if(err) {console.log('Error in Retriving User: ' + JSON.stringify(err, undefined, 2));}
                        else {
                            voluntary.volunteers.forEach(volunteeer => {
                                if(volunteeer.user._id.toString() != user_id.toString()){
                                    volunteers_involved.push(volunteeer);
                                }
                            });
                        }

                        us.volunteers = volunteers_involved;

                        Voluntary.findByIdAndUpdate(req.params.id, us , (err, doc) => {
                            if(!err) {res.send(doc); }
                            else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
                        }); 
                    });
                }
            });
        }
        else { console.log('Error in Retriving Volunteers :' + JSON.stringify(err, undefined, 2))};
    });
});




// localhost:8081/api/voluntary/:id/volunteerDeroll
// Retirar o voluntario logado ao voluntariado recebido por id por req.paramss
router.get('/:id/volunteerDeroll', passport.authenticate('jwt', { session: false }), (req, res) => {
    var user_id = req.user._id;
    
    var us = {}

    var volunteers_involved = [];

    var volunteerStatic = {};

    Volunteer.find((err,volunteerss) => {
        if(!err) { 
            volunteerss.forEach(volunteer1 => {
                if(volunteer1.user._id.toString() == user_id.toString()){

                    Voluntary.findById(req.params.id, (err, voluntary) => {
                        if(err) {console.log('Error in Retriving User: ' + JSON.stringify(err, undefined, 2));}
                        else {
                            voluntary.volunteers.forEach(volunteeer => {
                                if(volunteeer.user._id.toString() != user_id.toString()){
                                    volunteers_involved.push(volunteeer);
                                }
                            });
                        }

                        us.volunteers = volunteers_involved;

                        Voluntary.findByIdAndUpdate(req.params.id, us , (err, doc) => {
                            if(!err) {res.send(doc); }
                            else { console.log('Error in User update: ' + JSON.stringify(err, undefined, 2)); }
                        }); 
                    });
                }
            });
        }
        else { console.log('Error in Retriving Volunteers :' + JSON.stringify(err, undefined, 2))};
    });
});



// => localhost:8081/api/voluntary/_id
// Apaga um voluntariado
router.delete('/:id', (req, res) => {
    Voluntary.findByIdAndDelete(req.params.id, (err, doc) => {
        if(!err) {res.json({success: true, message: 'Voluntariado apagado!'})}
        else { res.json({success: false, message: 'Ocorreu um erro ao apagar o voluntariado!', message: err})}
    });
});

router.get('/logged/voluntaries/:userType', passport.authenticate('jwt', { session: false }), (req, res) => {
    var userType = req.params.userType
    const user_voluntaries = [];
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            Voluntary.find((err, voluntaries) => {
                if(!err) {
                    voluntaries.forEach(voluntary => {
                        if(userType == 'Volunteer'){
                            voluntary.volunteers.forEach(volunteer => {
                                if(JSON.stringify(volunteer.user._id) === JSON.stringify(user._id)){
                                    user_voluntaries.push(voluntary);
                                }
                            });
                        } else if (userType == 'Entity'){
                            if(voluntary.entity.user &&  JSON.stringify(voluntary.entity.user._id) === JSON.stringify(user._id)){
                                user_voluntaries.push(voluntary);
                            }
                        }
                    });
                }
                else { console.log('Error in Retriving Voluntary :' + JSON.stringify(err, undefined, 2))};
                Proposal.find((err, proposals) => {
                    if(!err) {
                        proposals.forEach(proposal => {
                            if (userType == 'Entity'){
                                if(proposal.voluntary.entity.user &&  JSON.stringify(proposal.voluntary.entity.user._id) === JSON.stringify(user._id)){
                                    user_voluntaries.push(proposal);
                                }
                            }
                        });
                    }
                    else { console.log('Error in Retriving Voluntary :' + JSON.stringify(err, undefined, 2))};
                    res.json({success: true, values: user_voluntaries});
                });
            });
        }
    });
});

router.get('/voluntaries/:id/:userType', (req, res) => {
    var userType = req.params.userType
    const user_voluntaries = [];
    User.findOne({ _id: req.params.id }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            Voluntary.find((err, voluntaries) => {
                if(!err) {
                    voluntaries.forEach(voluntary => {
                        if(userType == 'Volunteer'){
                            voluntary.volunteers.forEach(volunteer => {
                                if(JSON.stringify(volunteer.user._id) === JSON.stringify(user._id)){
                                    user_voluntaries.push(voluntary);
                                }
                            });
                        } else if (userType == 'Entity'){
                            if(voluntary.entity.user &&  JSON.stringify(voluntary.entity.user._id) === JSON.stringify(user._id)){
                                user_voluntaries.push(voluntary);
                            }
                        }
                    });
                }
                else { console.log('Error in Retriving Voluntary :' + JSON.stringify(err, undefined, 2))};
                Proposal.find((err, proposals) => {
                    if(!err) {
                        proposals.forEach(proposal => {
                            if (userType == 'Entity'){
                                if(proposal.voluntary.entity.user &&  JSON.stringify(proposal.voluntary.entity.user._id) === JSON.stringify(user._id)){
                                    user_voluntaries.push(proposal);
                                }
                            }
                        });
                    }
                    else { console.log('Error in Retriving Voluntary :' + JSON.stringify(err, undefined, 2))};
                    res.json({success: true, values: user_voluntaries});
                });
            });
        }
    });
});

router.get('/isUserInvolved/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            Voluntary.findOne({ _id: req.params.id }).select().exec((err, voluntary) => {
                if(err){
                    throw err;
                }
                if(voluntary) {
                    var found_user = false;
                    voluntary.volunteers.forEach(element => {
                        if(element.user._id.toString() == user._id.toString()){
                            found_user = true;
                        }
                    });
                    if(found_user){
                        res.json({success: true, message:'O utilizador está inscrito no voluntariado'});
                    } else {
                        res.json({success: false, message:'O utilizador não está inscrito no voluntariado'});
                    }
                } else {
                    res.json({success: false, message: 'Não existe este voluntariado'});
                }
            });
        }
    });
});

router.post('/createAfterAccept', (req, res) => {
    console.log("req.body.voluntary: " + req.body.voluntary)
    console.log("voluntaryName: " + req.body.voluntary.voluntaryName)
    var newVoluntary = new Voluntary({
        voluntaryName: req.body.voluntary.voluntaryName,
        entity: req.body.voluntary.entity,
        description: req.body.voluntary.description,
        area: req.body.voluntary.area,
        goals: req.body.voluntary.goals,
        targetAudience: req.body.voluntary.targetAudience,
        activityDescription: req.body.voluntary.activityDescription,
        specificTraining: req.body.voluntary.specificTraining,
        voluntary_areas: req.body.voluntary.voluntary_areas,
        observations: req.body.voluntary.observations,
        entitiesInvolved: req.body.voluntary.entitiesInvolved
    });
    console.log("new Voluntary: " + newVoluntary)

    newVoluntary.save((err, doc) => {   
        if(!err){
            res.json({ success: true, message: 'Voluntário criado!', value: newVoluntary});
        } else {
            res.json({ success: false, message: err});
        }
    });
});

module.exports = router;