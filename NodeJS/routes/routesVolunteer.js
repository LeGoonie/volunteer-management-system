const express = require('express');
const router = express.Router();
const Volunteer = require('../models/volunteer');
const Voluntary = require('../models/voluntary');
const User = require('../models/user');
const sendgridConfig = require('../config/sendgrid');
const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(sendgridConfig.api_key);

router.post('/', (req, res) => {
    var new_user_id = req.body.user_id;
    var newVolunteer = new Volunteer();

    User.findOne({ _id: req.body.user_id}).select().exec((err, user) => {
        if(err) {
            throw err;
        }
        if(user) {
            newVolunteer.user=user;
        
        

            newVolunteer.typeOfMember = req.body.typeOfMember;
            newVolunteer.course = req.body.course;
            newVolunteer.observations = req.body.observations;
            newVolunteer.interestAreas = req.body.interestAreas;
            newVolunteer.reasonsVolunteer = req.body.reasonsVolunteer;
            newVolunteer.schoolService = req.body.schoolService;

            newVolunteer.save((err, doc) => {
                if(!err){
                    res.json({ sucess: true, message: 'Volunteer created'});
                } else {
                    res.json({sucess: false, message: 'Utilizador não existe!'});
                }
            });
        } else { 
            res.json({success: false, message: 'Utilizador não existe!'})
            }
    });
});

// => localhost:8081/api/volunteer/volunteers
// Retornas todos voluntarios
router.get('/', (req, res) => {
    Volunteer.find((err,docs) => {
        if(!err) { res.send(docs); }
        else { console.log('Error in Retriving Volunteers :' + JSON.stringify(err, undefined, 2))};
    });
});


// => localhost:8081/api/volunteer/_id
// Retorna apenas 1 Volunteer
router.get('/:id', (req,res)=>{
    Volunteer.findById(req.params.id, (err, doc) => {
    if(!err) {res.send(doc); }
    else { 
        console.log('Error in Retriving Volunteer: ' + JSON.stringify(err, undefined, 2));
        res.send(err); 
        }
    });        
});


// => localhost:8081/api/volunteer/_id
// Modifica uma Volunteer
router.put('/:id', (req, res) => {
    var vo = {};
    vo.user_id = req.body.user_id;
    vo.typeOfMember = req.body.typeOfMember;
    vo.course = req.body.course;
    vo.observations = req.body.observations;
    vo.interestAreas = req.body.interestAreas;
    vo.reasonsVolunteer = req.body.reasonsVolunteer;
    
    Volunteer.findByIdAndUpdate(req.params.id, vo , (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in Volunteer update: ' + JSON.stringify(err, undefined, 2));
        res.send(err); 
        }
    }); 
});


// => localhost:8081/api/entity/_id
// Apaga uma entidade
router.delete('/:id', (req, res) => {
        Volunteer.findByIdAndDelete(req.params.id, (err, doc) => {
        if(!err) {res.send(doc); }
        else { console.log('Error in Volunteer Delete: ' + JSON.stringify(err, undefined, 2));
        res.send(err); 
        }
    });
});

// => localhost:8081/api/volunteer/findByInterestArea
// Encontra todos os utilizadores que tenham interesse em pelo menos uma das áreas que são fornecidas por "body"
router.post('/findBy/InterestArea', (req, res) => {
    var volunteers_emails = [];
    Volunteer.find((err, volunteers) => {
        if(err){ 
            throw err; 
        } else { 
            volunteers.forEach(volunteer => {
                volunteer.interestAreas.forEach(interestArea => {
                    req.body.areas.forEach(area => {
                        if(interestArea == area){
                            volunteers_emails.push(volunteer.user.email);
                        }
                    });
                });
            });

            volunteers_emails.forEach(email => {
                const msg = {
                    to: email,
                    from: 'volunteer.platform.ips@gmail.com',
                    templateId: 'd-49aecc07e0f843378ffe6ee6e3c2a7e0',
                    dynamic_template_data: {
                        subject: 'Notificação do sistema de gestão de voluntariados',
                        voluntary_id: req.body.voluntaryId
                    }
                };
                sgMail.send(msg).then(() => {
                    console.log('Message sent')
                }).catch((err) => {
                    console.log(err.response.body)
                })
            });
        }
        res.json({success: true, message: 'Emails enviados para utilziadores com áreas de interesse em comum'})
    })
});



module.exports = router;