const {MongoClient} = require('mongodb');
const config = require('../config/database.js');

describe('insert', () => {
  let connection;
  let db;
  var id = Math.random();

  

  const mockUser = {
  _id: id,
  admin: "false",
  active: true,
  fullName: "André Reis",
  email: "andrereis992222@gmail.com",
  phone: 123456789,
  address: "Rua das Mimosas",
  birthDate:"1999-07-19T00:00:00.000Z",
  password: "$2a$10$9fjK3nTcjTb8DZWhZsvr3edBogrBw9E5M/Z7CHP13F7F7O1b4TH2C",
  photoPath: "../../../assets/images/IMG_20200518_134931.jpeg",
  temporaryToken: "false",};

  const mockEntity = {
    _id: id,
    user: mockUser,
    nome: "Ai gonçalo",
    description: "ow mai god",
  };

  const mockVoluntary = {
    _id : id,
    volunteers: [mockUser],
    entity: "123rinf4i2g5r42",
    date: "1999-07-19T00:00:00.000Z",
    entitiesInvolved: [mockEntity],
    description: "alguma descriçao",
    area: "EI",
    goals: "bombar isso tudo",
    targetAudience: "gonçalo olha os palavrões na base de dados",
    activityDescription: "fazer alguma cena",
    specificTraining: "treino",
    voluntary_areas: ["Carapau", "Bacalhau"],
    observations: "Não pode ter pés mal cheirosos",
  };

  const mockProposal = {
    _id: id,
    user: mockUser,
    voluntary: mockVoluntary,
    state: false,
    submissionDate: "2020-05-23T16:53:18.392Z"
  };

  beforeAll(async () => {
    connection = await MongoClient.connect('mongodb+srv://Goonie:' + config.secret + '@cluster0-k4mp8.mongodb.net/VolunteerSystemDB?retryWrites=true&w=majority', {
      useNewUrlParser: true,
    });
    db = await connection.db('VolunteerSystemDB');
  });

  afterAll(async () => {
    await connection.close();
    await db.close();
  });

  

  it('should insert a user into users', async () => {
    const users = db.collection('users');
    await users.insertOne(mockUser);
    const insertedUser = await users.findOne({_id: id});
    expect(insertedUser).toEqual(mockUser);
  });

  it('should insert a entity into entities', async () => {
    const entities = db.collection('entities');
    await entities.insertOne(mockEntity);
    const insertedEntity = await entities.findOne({_id: id});
    expect(insertedEntity).toEqual(mockEntity);
  });

  it('should insert a voluntary into voluntaries', async () => {
    const voluntaries = db.collection('voluntaries');
    await voluntaries.insertOne(mockVoluntary);
    const insertedVoluntary = await voluntaries.findOne({_id: id});
    expect(insertedVoluntary).toEqual(mockVoluntary);
  });

  it('should insert a proposal into proposals', async () => {
    const proposals = db.collection('proposals');
    await proposals.insertOne(mockProposal);
    const insertedProposal = await proposals.findOne({_id: id});
    expect(insertedProposal).toEqual(mockProposal);
  });
});