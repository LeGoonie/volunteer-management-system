const request = require("supertest");
const app = require("../app.js");

describe("Test the get of a user", () => {
    test("It should response the GET method of a user", done => {
      request(app)
        .get("/api/users/5eb97025dae661214416879f")
        .then((response => {
          expect(response.statusCode).toBe(200);
          done();
        }));
    });
});

describe("Test the get of a entity", () => {
  test("It should response the GET method of a entity", done => {
    request(app)
      .get("/api/entity/5ecc6104b715564810c9b5cd")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of a voluntary", () => {
  test("It should response the GET method of a voluntary", done => {
    request(app)
      .get("/api/voluntary/5ecbe3979f9824460c7d57af")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of a volunteer", () => {
  test("It should response the GET method of a volunteer", done => {
    request(app)
      .get("/api/volunteer/5ecbd4783286763fb8206bfd")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of a proposal", () => {
    test("It should response the GET method of a proposal", done => {
      request(app)
        .get("/api/proposal/5ec93d4ad560545cb848d0c6")
        .then((response => {
          expect(response.statusCode).toBe(200);
          done();
        }));
    });
});

describe("Test the login", () => {
  test("It should response the login", async(done) => {
    const login = {
      email: "andrereis992@gmail.com",
      password: "same"
    }
    await request(app).post("/api/login").send(login)
      .then((response => {
        expect(response.body.success).toBe(true);
        done();
      }));
  });
});

/*describe("Test the register", () => {
  test("It should response the register", async(done) => {
    const register = {
      fullName: "BrunoTeste",
      email: "emailteste@gmail.com",
      phone: "123456789",
      address: "rua",
      birthDate: "1999-07-19T00:00:00.000Z",
      password: "same123",
      passConfirmed: "same123",
      admin: "false",
    }
    await request(app).post("/api/register").send(register)
      .then((response => {
        expect(response.body.sucess).toBe(true);
        done();
      }));
  });
});*/

describe("Test the get of a profile", () => {
  test("It should response the GET method of a profile of another user", done => {
    request(app)
      .get("/api/profile/5ec93d4ad560545cb848d0c6")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of a user by his name", () => {
  test("It should response the GET of a user by his name", done => {
    request(app)
      .get("/api/getByName/André")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the suspension of a user", () => {
  test("It should suspend the user ", done => {
    request(app)
      .get("/api/suspend/5edbc3301cf19530e4c8a1c7")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
      
  });
  test("It should unsuspend the user ", done => {
    request(app)
      .get("/api/unsuspend/5edbc3301cf19530e4c8a1c7")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of voluntaries", () => {
  test("It should response the GET of voluntaries", done => {
    request(app)
      .get("/api/voluntary/")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of proposals of certain entity", () => {
  test("It should response the GET of proposals of certain entity", done => {
    request(app)
      .get("/api/proposal/entityProposals/5ed273cb455f653b88201de1")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of proposals of a certain user", () => {
  let token = 'some_token';
  test("It should response the login", async(done) => {
    const login = {
      email: "andrereis992@gmail.com",
      password: "same"
    }
    await request(app).post("/api/login").send(login)
      .then((response => {
        var original = response.body.token;
        token = original.substr(original.indexOf(" ") + 1);
        expect(response.body.success).toBe(true);
        done();
      }));
  });
  test("It should response the proposals of the user", async(done) => {
    request(app)
      .get("/voluntary/logged/voluntaries/Volunteer")
      .set({ "Authorization": `Bearer ${token}` })
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of user involved in voluntary", () => {
  let token = 'some_token';
  test("It should response the login", async(done) => {
    const login = {
      email: "andrereis992@gmail.com",
      password: "same"
    }
    await request(app).post("/api/login").send(login)
      .then((response => {
        var original = response.body.token;
        token = original.substr(original.indexOf(" ") + 1);
        expect(response.body.success).toBe(true);
        done();
      }));
  });
  test("It should respond if the user is involved", async(done) => {
    request(app)
      .get("/voluntary/isUserInvolved/5ed7c60cb07e493acc8771e7")
      .set({ "Authorization": `Bearer ${token}` })
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of the entitie of the user involved", () => {
  let token = 'some_token';
  test("It should response the login", async(done) => {
    const login = {
      email: "bruno.goonie@hotmail.com",
      password: "same123"
    }
    await request(app).post("/api/login").send(login)
      .then((response => {
        var original = response.body.token;
        token = original.substr(original.indexOf(" ") + 1);
        expect(response.body.success).toBe(true);
        done();
      }));
  });
  test("It should respond the entity involved", async(done) => {
    request(app)
      .get("/entity/logged_entity/entity")
      .set({ "Authorization": `Bearer ${token}` })
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of the enroll and deroll of a user into a voluntary", () => {
  let token = 'some_token';
  test("It should response the login", async(done) => {
    const login = {
      email: "bruno.goonie@gmail.com",
      password: "same123"
    }
    await request(app).post("/api/login").send(login)
      .then((response => {
        var original = response.body.token;
        token = original.substr(original.indexOf(" ") + 1);
        expect(response.body.success).toBe(true);
        done();
      }));
  });
  test("It should respond the enroll of the volunteer", async(done) => {
    request(app)
      .get("/voluntary/5ee5394cecbd323a8447753a/volunteerEnroll")
      .set({ "Authorization": `Bearer ${token}` })
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should respond the deroll of the volunteer", async(done) => {
    request(app)
      .get("/voluntary/5ee5394cecbd323a8447753a/volunteerDeroll")
      .set({ "Authorization": `Bearer ${token}` })
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of the voluntaries of the user that is logged", () => {
  let token = 'some_token';
  test("It should response the login", async(done) => {
    const login = {
      email: "andrereis992@gmail.com",
      password: "same"
    }
    await request(app).post("/api/login").send(login)
      .then((response => {
        var original = response.body.token;
        token = original.substr(original.indexOf(" ") + 1);
        expect(response.body.success).toBe(true);
        done();
      }));
  });
  test("It should respond the GET of the voluntaries of the user logged", async(done) => {
    request(app)
      .get("/voluntary/logged/voluntaries/Volunteer")
      .set({ "Authorization": `Bearer ${token}` })
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of the notifications of a user", () => {
  test("It should response the GET method of a the notifications of a user", done => {
    request(app)
      .get("/api/notification/5ec93d4ad560545cb848d0c6")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of a certain notification with its id", () => {
  test("It should response the GET method of a the notification with its id", done => {
    request(app)
      .get("/api/notification/notificationId/5ee7809349eaff39a05a7a70")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the post of a notification", () => {
  test("It should response the post of a notification", async(done) => {
    const newNotification = {
      title: "Notificação lixada",
      text: "Notificação dos testes",
      state: "Nova",
      user_id: "5ebfd215d6bb6b36f898cb32"
    }
    await request(app).post("/api/notification").send(newNotification)
      .then((response => {
        expect(response.body.state).toBe("Nova");
        done();
      }));
  });
});

describe("Test the get of all notifications", () => {
  test("It should response the GET method of aall notifications", done => {
    request(app)
      .get("/api/notification/")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});

describe("Test the get of all statistics", () => {
  test("It should response the GET method of Numero total de entidades ", done => {
    request(app)
      .get("/api/statistics/entity/total")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of Numero total de volunteers ", done => {
    request(app)
      .get("/api/statistics/volunteer/total")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of Numero total de proposals ", done => {
    request(app)
      .get("/api/statistics/proposals/total")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("Retorna a percentagem de voluntariados aceites, rejeitados e a aguardar aprovação ", done => {
    request(app)
      .get("/api/statistics/proposals/acceptedRejectedPending")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of Numero total de users ", done => {
    request(app)
      .get("/api/statistics/users/total")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of Numero total de admins ", done => {
    request(app)
      .get("/api/statistics/users/admin/total")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of Numero total de voluntariados ", done => {
    request(app)
      .get("/api/statistics/voluntaries/total")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of numero medio de voluntarios por voluntariado ", done => {
    request(app)
      .get("/api/statistics/voluntaries/averagePerVoluntary")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
  test("It should response the GET method of numero de areas voluntariado num dicionario ", done => {
    request(app)
      .get("/api/statistics/voluntaries/areas/voluntaries")
      .then((response => {
        expect(response.statusCode).toBe(200);
        done();
      }));
  });
});


/*describe("Test the POST of a voluntary", () => {
  test("It should response the POST of a voluntary", async(done) => {
    const voluntary = {
      voluntaryName: "AcceptTestev1",
      volunteers: [{
        user: {
            admin: false,
            active: false,
            suspended: false,
            _id:"5ee63cdc2d99e84f8022ba7c",
            fullName: "Duarte Gonçalves",
            email: "redmundpt1@gmail.com",
            phone: 937543535,
            address: "Rua José Elias Garcia, 54 4ºandar",
            birthDate:"1999-09-28T00:00:00.000Z",
            password: "$2a$10$Zg9fZFqh4c1wiYdzfEB3ZOv16uppg3YqxLhwlL.DHgkp561rr/zdS",
            photoPath: "../../../assets/images/default_profile.png",
            temporaryToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkR1YXJ0ZSBHb27Dp2FsdmVzIiwiZW1haWwiOiJyZWRtdW5kcHQxQGdtYWlsLmNvbSIsImlhdCI6MTU5MjE0NzE2NCwiZXhwIjoxNTkyMjMzNTY0fQ.KXM5WqvgZgPP1IxCrre_Z0iZfBZrTBR9MxfRsM6V3OI"
        },
        schoolService: "IPS",
        course: "Engenharia Informática",
        observations: "Same",
        interestAreas: ["Atividades Académicas (por ex. apoio às matrículas…)"],
        reasonsVolunteer: ["Pelo convívio social"],
        _id: "5ee63cdd2d99e84f8022ba7d",
        typeOfMember: "Estudante"
      }],
      entity: new Object(),
      description: "asd",
      area: "area",
      goals: "req.body.goals",
      targetAudience: "req.body.targetAudience",
      activityDescription: "req.body.activityDescription",
      specificTraining: "req.body.specificTraining",
      entitiesInvolved: ["Don Dimon", "Fabrica do primo"],
      voluntary_areas: ["Atividades Académicas (por ex. apoio às matrículas…)", "Ambiental (por ex. ações de sensibilização"],
      observations: "req.body.observations",
      date: new Date()
    }
    await request(app).post("/api/voluntary").send(voluntary)
      .then((response => {
        expect(response.body.sucess).toBe(true);
        done();
      }));
  });
});*/



/*describe("Test the post of a proposal", () => {
  test("It should respond to the proposal", async(done) => {
    const proposal = {
        voluntary: {
            volunteers: [],
            entity: {
                user: {
                    admin: false,
                    active: false,
                    suspended: false,
                    _id: "5ed5059b61ea2d142cc0faa7",
                    fullName: "Primo",
                    email: "redmundpt@gmail.com",
                    phone: 925406253,
                    address: "num sitio",
                    birthDate: "1999-07-19T00:00:00.000Z",
                    password: "$2a$10$DVV8zivnJiG/uZIqCpGKMeoU6VpzulgEB0hAFW9yV8CsJ3hQ8LXTi",
                    photoPath: "../../../assets/images/default_profile.png",
                    temporaryToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlByaW1vIiwiZW1haWwiOiJyZWRtdW5kcHRAZ21haWwuY29tIiwiaWF0IjoxNTkxMDE4OTA3LCJleHAiOjE1OTExMDUzMDd9.vhSsPGefdobrhVUGgxCpc0WXB4Al1ItcKy_-dXEv1-o",
                },
                _id: "5ed5059c61ea2d142cc0faa8",
                nome: "Fábrica",
                description: "Negócios",
            },
            description: "asd",
            date: "2020-06-15T00:00:00.000Z",
            entitiesInvolved: [
                "Don Dimon",
                "Fabrica do primo"
            ],
            voluntary_areas: [
                "Atividades Académicas (por ex. apoio às matrículas…)"
            ],
            _id: "5ee7a313dfd2485068e556a0",
            voluntaryName: "TesteProposalProfile",
            area: "Engenharia Informática",
            goals: "asd",
            targetAudience: "PublicoExemplo",
            activityDescription: "DescriçaoAtividadeExemplo",
            specificTraining: "FormaçãoExemplo",
            observations: "asd"
        },
        state: "Pendente",
        submissionDate: "2020-06-15T16:34:27.214Z"
  };
    await request(app).post("/api/proposal").send(proposal)
      .then((response => {
        expect(response.body.success).toBe(true);
        done();
      }));
  });
});*/
